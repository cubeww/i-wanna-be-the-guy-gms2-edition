{
    "id": "d0be43c2-550b-4203-be7d-d314af68cb2c",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tIntroTiles2",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 1,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "4bcd3db2-3ba6-4a4e-b980-9f5cc3a5c1c8",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 2,
    "tileheight": 64,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}