{
    "id": "f7ea90e1-21eb-4752-b63f-d53130920655",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuCloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 111,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76615b69-8ae7-49a4-aed7-99d0d45a8f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ea90e1-21eb-4752-b63f-d53130920655",
            "compositeImage": {
                "id": "9e354da8-94dd-450a-8958-18829095f0fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76615b69-8ae7-49a4-aed7-99d0d45a8f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cc9b3d3-86c4-4d4a-8d92-7beb0223c30d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76615b69-8ae7-49a4-aed7-99d0d45a8f96",
                    "LayerId": "dd37e68e-b077-4fa8-bf12-cdf7029edf10"
                }
            ]
        },
        {
            "id": "8ec358aa-a65f-4dd7-a9d3-cfc0291a99b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ea90e1-21eb-4752-b63f-d53130920655",
            "compositeImage": {
                "id": "69aa1337-95fe-4952-aaf3-6248a261d09d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec358aa-a65f-4dd7-a9d3-cfc0291a99b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d48ea959-927f-4e88-ac8d-75f330219d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec358aa-a65f-4dd7-a9d3-cfc0291a99b8",
                    "LayerId": "dd37e68e-b077-4fa8-bf12-cdf7029edf10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd37e68e-b077-4fa8-bf12-cdf7029edf10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7ea90e1-21eb-4752-b63f-d53130920655",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 112,
    "xorig": 0,
    "yorig": 0
}