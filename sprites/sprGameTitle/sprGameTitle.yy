{
    "id": "e2dbb5e1-8f55-4254-881c-9e22232842b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGameTitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 296,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cd27950-57b1-4609-bb56-eb148e83b216",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e2dbb5e1-8f55-4254-881c-9e22232842b0",
            "compositeImage": {
                "id": "dc109908-6688-43d0-aa68-50d5ab6663fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cd27950-57b1-4609-bb56-eb148e83b216",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d83e8881-c13f-45df-8edc-2b4d90adb79e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cd27950-57b1-4609-bb56-eb148e83b216",
                    "LayerId": "0b27318b-adde-4645-a27e-71663d5e6a25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 297,
    "layers": [
        {
            "id": "0b27318b-adde-4645-a27e-71663d5e6a25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e2dbb5e1-8f55-4254-881c-9e22232842b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}