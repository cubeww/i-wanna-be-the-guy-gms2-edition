{
    "id": "b52f6203-7dd0-45c5-9f2c-7d584eead5cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 351,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "553f2968-a26e-4df9-9a3c-120c59be2e4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b52f6203-7dd0-45c5-9f2c-7d584eead5cf",
            "compositeImage": {
                "id": "1a79c5b8-1014-4a77-883f-f78128fd679f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553f2968-a26e-4df9-9a3c-120c59be2e4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36238194-d538-4f74-8841-693ca7f593ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553f2968-a26e-4df9-9a3c-120c59be2e4c",
                    "LayerId": "687247b6-3b03-441c-8a48-537b3dc08b48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 352,
    "layers": [
        {
            "id": "687247b6-3b03-441c-8a48-537b3dc08b48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b52f6203-7dd0-45c5-9f2c-7d584eead5cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}