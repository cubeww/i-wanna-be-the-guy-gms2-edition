{
    "id": "4ef0a337-86b2-43e8-bf21-c870f4a26e63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuTree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 6,
    "bbox_right": 116,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "275143ba-cd40-4453-ab02-08f3a473496a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ef0a337-86b2-43e8-bf21-c870f4a26e63",
            "compositeImage": {
                "id": "b3c9984a-f8af-486f-8c99-fc97c46ac320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "275143ba-cd40-4453-ab02-08f3a473496a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89bed435-b093-43ef-a107-d25c92fb91c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "275143ba-cd40-4453-ab02-08f3a473496a",
                    "LayerId": "22fc8233-5372-4fb6-87f6-1965ed808136"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "22fc8233-5372-4fb6-87f6-1965ed808136",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ef0a337-86b2-43e8-bf21-c870f4a26e63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 121,
    "xorig": 0,
    "yorig": 0
}