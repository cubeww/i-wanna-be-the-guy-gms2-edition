{
    "id": "50aafe9f-c968-4eb4-a294-e16a5e80f90a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroTiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d496bead-a513-49d4-9586-ad4503cb4f65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50aafe9f-c968-4eb4-a294-e16a5e80f90a",
            "compositeImage": {
                "id": "a3b7da03-7b80-4b56-a94c-c1af5cbdf091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d496bead-a513-49d4-9586-ad4503cb4f65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27e679c3-21aa-4b14-8eea-4355fe137b40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d496bead-a513-49d4-9586-ad4503cb4f65",
                    "LayerId": "340fa775-24df-4c47-9b7a-ce887334232d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "340fa775-24df-4c47-9b7a-ce887334232d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50aafe9f-c968-4eb4-a294-e16a5e80f90a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}