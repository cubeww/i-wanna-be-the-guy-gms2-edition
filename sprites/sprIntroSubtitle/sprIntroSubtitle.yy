{
    "id": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroSubtitle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 164,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3f67be9-4eb9-4946-9992-62880a75065d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "ed0b8d3e-1a24-45b6-b111-338936869749",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f67be9-4eb9-4946-9992-62880a75065d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c83f1f-d2a7-424f-ae0c-661149834a25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f67be9-4eb9-4946-9992-62880a75065d",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "b46d8a77-ce4d-4e52-bc58-fb582e7d04cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "f12b843d-09a5-4537-af1d-870b12d31847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b46d8a77-ce4d-4e52-bc58-fb582e7d04cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f9dec25-d6e8-457b-bca0-e1bbd6b83fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b46d8a77-ce4d-4e52-bc58-fb582e7d04cb",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "c1e762e4-54d7-4ff4-a0b0-a08edcec8bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "709cface-e361-468d-a965-9d613cdbbd2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1e762e4-54d7-4ff4-a0b0-a08edcec8bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ef721d-be65-40f4-b1fa-7cda4fa155c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1e762e4-54d7-4ff4-a0b0-a08edcec8bbc",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "e87a23c4-c03b-464f-87a4-7b255362c465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "2c1931cb-5ae1-4f37-bdb3-08243f392f19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e87a23c4-c03b-464f-87a4-7b255362c465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c7ffa5-79fe-469c-b933-addee2502c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e87a23c4-c03b-464f-87a4-7b255362c465",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "bb1b7b8f-47db-43a7-a013-166dfbf2f97c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "6f756aa7-adcc-4463-89c8-1b7c865fad6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1b7b8f-47db-43a7-a013-166dfbf2f97c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d64ad8f0-aa83-46fc-b52b-723404fb6626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1b7b8f-47db-43a7-a013-166dfbf2f97c",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "51532890-2f01-45d7-b530-ac76725fad37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "2ea30ed7-74a4-4027-bf3b-f8319c2d25c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51532890-2f01-45d7-b530-ac76725fad37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac08dc7-cbbc-456c-9c6d-0874a4914451",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51532890-2f01-45d7-b530-ac76725fad37",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "5c64ce44-531a-4f0a-b03f-28594ca21ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "0283f3f5-a604-45a5-8f14-06aba9aebcbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c64ce44-531a-4f0a-b03f-28594ca21ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dfccba3-f35c-4b30-8f28-628cc624d777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c64ce44-531a-4f0a-b03f-28594ca21ff1",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "027f7e84-eba2-42b5-ba2f-5b4862fb49b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "2ea9daca-2d7c-405b-90db-9d8109910ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027f7e84-eba2-42b5-ba2f-5b4862fb49b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "231f22bc-cd6a-4fff-b959-612cd94df3df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027f7e84-eba2-42b5-ba2f-5b4862fb49b7",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "915d7a17-87ca-43ee-b59e-6868f0832c16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "685f4e1e-885d-47a0-b2c1-cbaf28193056",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "915d7a17-87ca-43ee-b59e-6868f0832c16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0ed4a5-4705-4788-9840-3c1e98b680c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "915d7a17-87ca-43ee-b59e-6868f0832c16",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "cb7a15b1-3b7f-4794-bcb6-daff6098440a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "a7ca385d-b97c-41c1-b1bf-afa81f864dac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7a15b1-3b7f-4794-bcb6-daff6098440a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6af89e5-8a8c-4fbe-888c-49525550235c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7a15b1-3b7f-4794-bcb6-daff6098440a",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "01972a2b-0f6f-4cf1-a101-8c84fbeccc22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "416e3fed-c1fb-4ef9-b827-bfee0f95a13a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01972a2b-0f6f-4cf1-a101-8c84fbeccc22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b029a50c-e979-48b4-aeea-d7ff5bcab623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01972a2b-0f6f-4cf1-a101-8c84fbeccc22",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "2c423930-26f1-44d6-a2c0-be491d69ef6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "19b0dee0-2040-4232-80de-673b766e50f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c423930-26f1-44d6-a2c0-be491d69ef6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "411111f1-1ab5-46ac-8d18-3c972ec4edae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c423930-26f1-44d6-a2c0-be491d69ef6b",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "54f87998-5a71-4290-8112-f027af732cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "220dec5d-0ba3-42ac-9e5d-f931b854e799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54f87998-5a71-4290-8112-f027af732cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e8d340-ed53-4f66-800a-ad417f1dd073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54f87998-5a71-4290-8112-f027af732cd2",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "6092e059-7f9d-4bfd-8eae-1268b9ce116f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "d208d44b-bf48-4b79-b132-a64259233879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6092e059-7f9d-4bfd-8eae-1268b9ce116f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ae7ad1c-9a22-40c5-8de6-5f4701b109aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6092e059-7f9d-4bfd-8eae-1268b9ce116f",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "c8444cfd-3c6b-4b0d-83bf-24dc5176f798",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "0aeebf65-c5b5-4413-8af6-b37a9949086e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8444cfd-3c6b-4b0d-83bf-24dc5176f798",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b85e5ba-95b5-468c-a452-34c47d7ae613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8444cfd-3c6b-4b0d-83bf-24dc5176f798",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "5ffd5299-fdc3-49bf-bdd2-70a3b849be63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "bb5d724b-8560-4a4b-8537-ea15f19d6f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ffd5299-fdc3-49bf-bdd2-70a3b849be63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "460b042a-39cc-461f-8978-875679ca22ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ffd5299-fdc3-49bf-bdd2-70a3b849be63",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "eaa01ab8-70d2-4ec4-8bd2-47c364359d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "27a436d9-9e60-4fe3-b96f-62268fd4ee16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaa01ab8-70d2-4ec4-8bd2-47c364359d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a53d931a-42f6-478d-b981-b195ed2854f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaa01ab8-70d2-4ec4-8bd2-47c364359d99",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "7d8f15f3-5a28-4b55-8595-1600753eccf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "7657f452-6225-4afc-b288-7e467d95c382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8f15f3-5a28-4b55-8595-1600753eccf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b97e7a2b-c19c-4e74-97d5-00f5885db7a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8f15f3-5a28-4b55-8595-1600753eccf1",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "c664b926-fae6-47e7-92b1-c7384dcf98e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "41bf8329-9756-4a11-bf03-c94e86a37e91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c664b926-fae6-47e7-92b1-c7384dcf98e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "786dc67c-b9c0-4e7f-8334-164724f595d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c664b926-fae6-47e7-92b1-c7384dcf98e2",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "d955374e-e1c4-46ad-802b-ec383a5ad711",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "df61589d-c3c8-4551-b9ca-8134b3196fa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d955374e-e1c4-46ad-802b-ec383a5ad711",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e932e152-6cf8-4192-bca4-6b83464f718c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d955374e-e1c4-46ad-802b-ec383a5ad711",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "b84e6352-562e-42f1-86d6-3b6a8cee5858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "69f7c15a-9ca1-44cd-b208-4609779d274f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84e6352-562e-42f1-86d6-3b6a8cee5858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6728c1f2-1cd5-48c3-ab88-46466da1807f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84e6352-562e-42f1-86d6-3b6a8cee5858",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "c3e92880-17fd-44ca-9a79-e3d776ef5567",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "add71d22-2dd7-4d12-a4ad-6a4fd884f6b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e92880-17fd-44ca-9a79-e3d776ef5567",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b9e44b6-7b25-4a0a-a416-c7507a5b81f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e92880-17fd-44ca-9a79-e3d776ef5567",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "cc00c821-1ca1-4371-8f11-983c7f1dc48d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "aef175ab-03ff-420f-a00c-15201da4f1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc00c821-1ca1-4371-8f11-983c7f1dc48d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a22e85-7783-47d0-be4e-7ec6959f8e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc00c821-1ca1-4371-8f11-983c7f1dc48d",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "24a8d66a-eb41-480b-87b4-f849e71bb679",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "46a703a4-344f-49f2-bc1d-1b1bc838be3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24a8d66a-eb41-480b-87b4-f849e71bb679",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a3c7645-4ba7-4582-93d1-ddfe4f1f24c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24a8d66a-eb41-480b-87b4-f849e71bb679",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "fb96cab4-e82b-40ad-9dc7-5396128603c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "149f10c0-1b02-4301-b5dc-26b5eaae47e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb96cab4-e82b-40ad-9dc7-5396128603c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9759524a-3e68-435a-a8b1-598aeddc7c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb96cab4-e82b-40ad-9dc7-5396128603c4",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "8d63e77a-6d75-4713-afc7-832c1cc72097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "40fbb3d6-7a55-4d00-a11d-9e10638d765a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d63e77a-6d75-4713-afc7-832c1cc72097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c854c402-2134-4ebe-a810-64e8bb7357dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d63e77a-6d75-4713-afc7-832c1cc72097",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "6e81ff52-56fa-4580-abea-cc6909ef8dda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "9240a8af-70b4-4962-a4bb-e4c366775d88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e81ff52-56fa-4580-abea-cc6909ef8dda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3220fbb3-7400-45f4-8d5e-0f7b4bbf0f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e81ff52-56fa-4580-abea-cc6909ef8dda",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "0d6cb378-f550-4f73-a3e3-34c2d78a69a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "2dcb1bf8-5701-45c3-9c78-982a60ec8b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6cb378-f550-4f73-a3e3-34c2d78a69a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da52983c-ac22-416c-949f-8d2326eee1da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6cb378-f550-4f73-a3e3-34c2d78a69a4",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "9cdf31af-f40a-4383-b108-bebd9f4f777c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "f030fefe-dc8b-44c3-a1e6-140ac0997619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cdf31af-f40a-4383-b108-bebd9f4f777c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3f310f-5fd5-49a9-87df-c546e64ffcea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cdf31af-f40a-4383-b108-bebd9f4f777c",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "5762c6f4-dfb1-4ed8-b73f-c683f6d150d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "1b3686e2-b0f0-4f96-b113-9b50405da903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5762c6f4-dfb1-4ed8-b73f-c683f6d150d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbd598f6-4976-44e6-87ff-b2e7dc65e4b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5762c6f4-dfb1-4ed8-b73f-c683f6d150d2",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "a6d4f138-04d7-4838-86a4-a27064ce1f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "390f081b-cc01-4c04-8b99-eed04f443ec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6d4f138-04d7-4838-86a4-a27064ce1f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a5b223-d8c9-4456-bf26-918be853254c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6d4f138-04d7-4838-86a4-a27064ce1f90",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "3a4c15b4-bb78-4756-8a8a-a016b9689e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "fd98d9a7-a5c0-4b1a-aa52-3e4625fb8c7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a4c15b4-bb78-4756-8a8a-a016b9689e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b78f3ded-a4dd-48dd-878f-2a3e615996d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a4c15b4-bb78-4756-8a8a-a016b9689e3f",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "4b02e654-b713-4ca4-9cac-31faee5bd770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "b828a8b4-f852-458c-be21-ddc8d550e35b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b02e654-b713-4ca4-9cac-31faee5bd770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23e225a-221c-40d5-bd5a-4149abf3c9f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b02e654-b713-4ca4-9cac-31faee5bd770",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "e5687e52-c8e6-4d0a-9312-071980ed4e33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "6f29925d-21dc-4800-b90d-5aa0e10ff488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5687e52-c8e6-4d0a-9312-071980ed4e33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4bc90b7-944f-488f-a427-9718f8463c97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5687e52-c8e6-4d0a-9312-071980ed4e33",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "d347d4d6-f939-4252-87d1-ad33c74e8003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "03d26c52-97cb-4504-bee8-5061f6022894",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d347d4d6-f939-4252-87d1-ad33c74e8003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60727c3-e67b-4df9-a864-245e28f42821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d347d4d6-f939-4252-87d1-ad33c74e8003",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "88a327c9-9bc7-47aa-8fca-b766d54daf38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "e2a94144-4ad6-4b28-aca5-96665b663442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88a327c9-9bc7-47aa-8fca-b766d54daf38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42793724-4ed7-4d57-b293-96bff2098e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88a327c9-9bc7-47aa-8fca-b766d54daf38",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "6d837eed-8404-4953-b588-d1300ace6d0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "7deffc88-00e3-4930-8987-1c8a4b4042bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d837eed-8404-4953-b588-d1300ace6d0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13e89927-9dcf-4824-8b50-1c46c1861279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d837eed-8404-4953-b588-d1300ace6d0b",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "0b13a304-fa94-48ba-8cb7-94a174594383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "dfd1fc03-4582-4319-9fb0-243da0c45410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b13a304-fa94-48ba-8cb7-94a174594383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55ef8412-8d37-4616-92e5-ae0f8518cb8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b13a304-fa94-48ba-8cb7-94a174594383",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "e35cc4ef-8cdd-4dc7-a9cb-6eacebf47166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "eef6cbf5-b917-4128-a96f-dc634367accb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e35cc4ef-8cdd-4dc7-a9cb-6eacebf47166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dfde31b-f7d0-4dd8-9304-58a7d1538a2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e35cc4ef-8cdd-4dc7-a9cb-6eacebf47166",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "7dcd76ae-b8e0-4928-aef9-ffcbb1a15ef4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "f65b047f-ab87-4edc-b360-7f989c53bac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dcd76ae-b8e0-4928-aef9-ffcbb1a15ef4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9b7e61-6eb4-4ae7-a699-1e025159e058",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dcd76ae-b8e0-4928-aef9-ffcbb1a15ef4",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "3bd359cd-3db5-4432-8113-bbef9058347a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "7b44df9b-07f4-41d6-b6a2-f38e3f0f8110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd359cd-3db5-4432-8113-bbef9058347a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c014a369-c95a-4adb-97a7-79ba90c30b89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd359cd-3db5-4432-8113-bbef9058347a",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "eb84919b-afa3-4976-91a9-c4682409c509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "869ca266-51b3-4bb2-83db-c67638054fa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb84919b-afa3-4976-91a9-c4682409c509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91cf4426-aef7-4129-83b5-db6b4c601ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb84919b-afa3-4976-91a9-c4682409c509",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "c10a1bc4-d758-48e4-9af4-44573f0283fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "e73dcfd7-74f4-4ccb-a888-d5f2a131c68b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c10a1bc4-d758-48e4-9af4-44573f0283fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbf116d1-a2af-4fd0-8be2-9b5ada36bcf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c10a1bc4-d758-48e4-9af4-44573f0283fb",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "9b42a2d7-318d-4f6b-b68a-e2b639769999",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "dfd18c43-77ff-4ff4-b78a-7b54670dc27f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b42a2d7-318d-4f6b-b68a-e2b639769999",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89460c66-eafe-45e8-b5af-daefd6e12f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b42a2d7-318d-4f6b-b68a-e2b639769999",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "8a9522e4-5af8-4202-993f-6a27c8fceb8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "57d0d880-2b48-482b-846e-64946561bb35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a9522e4-5af8-4202-993f-6a27c8fceb8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2def2eb-1dc9-4520-a555-06bff832e68c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a9522e4-5af8-4202-993f-6a27c8fceb8b",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "d7ec6c32-ca27-4f96-8eea-903a68eb47ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "b0b0f7fe-5f97-417b-a777-2296344d5091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7ec6c32-ca27-4f96-8eea-903a68eb47ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbfc8899-2f83-4c42-89aa-30d38d504d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7ec6c32-ca27-4f96-8eea-903a68eb47ce",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "db55f2ca-f64e-4f63-bcb6-d4b212150ecf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "987b2870-a5e8-4d78-8b82-8abf0b94cf0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db55f2ca-f64e-4f63-bcb6-d4b212150ecf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a71fae8-c611-4c83-9d10-0378e3858f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db55f2ca-f64e-4f63-bcb6-d4b212150ecf",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "d898972f-e677-4a7a-b2ef-fb0bbea593e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "7d757ab7-8930-418c-a33d-c03e6491a214",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d898972f-e677-4a7a-b2ef-fb0bbea593e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc52fa0e-7332-471a-bcb8-621fec14e7ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d898972f-e677-4a7a-b2ef-fb0bbea593e1",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "a399221c-3765-4823-a106-05fd66213801",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "dc6ef6ce-a28a-4e7c-a36e-890ede1774a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a399221c-3765-4823-a106-05fd66213801",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c11309e-284a-4c00-a5d0-4068395d1e51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a399221c-3765-4823-a106-05fd66213801",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "34b84886-2a88-4779-9777-ed1e0b8ad1dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "1d7980cb-fa45-4c00-9516-93326e74fe9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b84886-2a88-4779-9777-ed1e0b8ad1dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d555498-1d98-44f3-8242-f1e046e2a4a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b84886-2a88-4779-9777-ed1e0b8ad1dc",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "63f8175c-1b8a-40fa-9575-52340f9cf3bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "80e54331-cac7-41b3-bb96-947c6be2b938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63f8175c-1b8a-40fa-9575-52340f9cf3bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3beedd5-e36a-400d-9653-186f1211eb90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63f8175c-1b8a-40fa-9575-52340f9cf3bc",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "b3b361cc-e77e-47a5-a5ef-5c75914c1928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "fb61e986-0c7b-4a8a-912a-63bdc7535e74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b361cc-e77e-47a5-a5ef-5c75914c1928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc494098-01e7-442a-a74c-d71ec6cf7030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b361cc-e77e-47a5-a5ef-5c75914c1928",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "0be807d0-2966-461a-87ae-c0807a4ca93d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "bd0dcea4-e54e-4fa9-a406-b6b585242ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0be807d0-2966-461a-87ae-c0807a4ca93d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53dd545e-7118-45a5-babb-3505601629a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0be807d0-2966-461a-87ae-c0807a4ca93d",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "3abf3a38-b476-41de-a20e-ba5e4f118611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "abb88682-c0ec-4d42-b593-8e0eec01cad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3abf3a38-b476-41de-a20e-ba5e4f118611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04e741e2-a453-4626-9e29-f2be8dd73493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3abf3a38-b476-41de-a20e-ba5e4f118611",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "2130301f-7b19-46dd-a83f-13460b8347ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "084c87d1-2c42-4d50-b13e-bd135590903a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2130301f-7b19-46dd-a83f-13460b8347ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5af4da4b-5ec7-424a-9ad8-e5b796918402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2130301f-7b19-46dd-a83f-13460b8347ac",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "3941642e-ebc0-4447-af36-c78084b905d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "8bdfcdaf-6c14-4bfd-ae1b-649ebb9038ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3941642e-ebc0-4447-af36-c78084b905d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc1dd0b5-5901-4864-99d7-c4ac608aa3e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3941642e-ebc0-4447-af36-c78084b905d2",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "69b447f7-baa5-4849-a073-6719ba5fbbee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "6f0b0a2b-5158-4436-b738-22f11132fd17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69b447f7-baa5-4849-a073-6719ba5fbbee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b2926e-0f39-4e55-8922-c34b17141ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69b447f7-baa5-4849-a073-6719ba5fbbee",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "8b978725-a36e-45e5-ab4d-21f18cee2667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "41dec3f0-ff61-43b9-a9db-116c8b847b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b978725-a36e-45e5-ab4d-21f18cee2667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4965eb4-c5fe-4006-a0a8-3de686cf7d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b978725-a36e-45e5-ab4d-21f18cee2667",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "095f5b59-4233-48d4-b4d2-0332008a30b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "3205272a-95c6-4f7b-bc76-264b95298ff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "095f5b59-4233-48d4-b4d2-0332008a30b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34354927-9a7b-4338-a33a-e0c69f9cb9c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "095f5b59-4233-48d4-b4d2-0332008a30b6",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "77e1a043-d228-4bc4-ba6b-9b5fa9ea7626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "a37c95e5-37b4-4caa-8ae4-c5ab3bbb01f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e1a043-d228-4bc4-ba6b-9b5fa9ea7626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35a9bc48-2631-4936-8b26-9e61ad1f9f3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e1a043-d228-4bc4-ba6b-9b5fa9ea7626",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "dec40f54-a504-4f40-97f9-9972a1cacc39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "dd01ba05-126d-4aa2-a792-6cee5308688c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec40f54-a504-4f40-97f9-9972a1cacc39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4e234d8-521e-4dd3-99fc-c82c7e4ec5e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec40f54-a504-4f40-97f9-9972a1cacc39",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        },
        {
            "id": "1eae5441-ef31-431e-b2c3-ea5eea54ad5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "compositeImage": {
                "id": "2ae6f0be-ffb0-4a21-85d1-725664b52750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eae5441-ef31-431e-b2c3-ea5eea54ad5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d854f02-7908-4cd4-ad78-73e5cbeb1e45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eae5441-ef31-431e-b2c3-ea5eea54ad5d",
                    "LayerId": "2ba69efa-4fc2-45ec-b8d0-4c3575380155"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 165,
    "layers": [
        {
            "id": "2ba69efa-4fc2-45ec-b8d0-4c3575380155",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf9a81d8-7b78-42bb-892f-b32f312aa407",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2.5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}