{
    "id": "52915684-11df-4602-88a6-a19c9b0711ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleStart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 0,
    "bbox_right": 126,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8aa935f-fe9b-485c-b83a-ddf0b7665cb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52915684-11df-4602-88a6-a19c9b0711ee",
            "compositeImage": {
                "id": "c808badd-5bce-4822-a488-014a53d07df8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8aa935f-fe9b-485c-b83a-ddf0b7665cb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1316c44c-4c48-48c0-bf3c-9071fb7207d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8aa935f-fe9b-485c-b83a-ddf0b7665cb0",
                    "LayerId": "57fcd22a-b489-46ae-8924-475ce4d81f0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "57fcd22a-b489-46ae-8924-475ce4d81f0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52915684-11df-4602-88a6-a19c9b0711ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 127,
    "xorig": 0,
    "yorig": 0
}