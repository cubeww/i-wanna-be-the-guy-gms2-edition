{
    "id": "6aef86c0-6aaf-4516-95a2-0252603fc52b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "749ad59d-c153-4bb9-9e39-eb825189c9f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6aef86c0-6aaf-4516-95a2-0252603fc52b",
            "compositeImage": {
                "id": "fc6a4d61-99da-4e75-acd4-7d165ff8eaef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749ad59d-c153-4bb9-9e39-eb825189c9f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2cc8dae-8679-42e6-bc0e-9211945a960b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749ad59d-c153-4bb9-9e39-eb825189c9f0",
                    "LayerId": "5dec107a-4d93-404f-922d-eaebc667f6fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "5dec107a-4d93-404f-922d-eaebc667f6fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6aef86c0-6aaf-4516-95a2-0252603fc52b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}