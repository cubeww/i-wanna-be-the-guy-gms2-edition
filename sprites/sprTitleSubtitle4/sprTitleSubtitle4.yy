{
    "id": "f480fcb1-2a7d-41c5-85d1-c266471d7d7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 205,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3b9432e-9de1-4278-840d-ddcbe7883e8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f480fcb1-2a7d-41c5-85d1-c266471d7d7c",
            "compositeImage": {
                "id": "087836e0-5a59-4eff-b610-9dd611f81671",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3b9432e-9de1-4278-840d-ddcbe7883e8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd7e6fd8-7042-4e62-b8cb-0c1606e29932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3b9432e-9de1-4278-840d-ddcbe7883e8d",
                    "LayerId": "b125b259-79ec-44d2-9f5b-07b690e15e26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 206,
    "layers": [
        {
            "id": "b125b259-79ec-44d2-9f5b-07b690e15e26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f480fcb1-2a7d-41c5-85d1-c266471d7d7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}