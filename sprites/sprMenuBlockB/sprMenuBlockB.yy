{
    "id": "7b53982f-2f60-4c77-9134-519e570ddbee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuBlockB",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "beb8a5b9-b791-423f-8109-8b4d6af28f4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "5bb6f05c-240b-4039-90ff-3b4114b93787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "beb8a5b9-b791-423f-8109-8b4d6af28f4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c299dff7-1bc2-4a0f-9753-eba1b839a735",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "beb8a5b9-b791-423f-8109-8b4d6af28f4e",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "958de08d-7800-4cc5-9389-8e8ae9f97740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "03489b16-a1dd-4df6-9d21-f956a86a16ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "958de08d-7800-4cc5-9389-8e8ae9f97740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d6c5c77-1e53-49ac-88a8-cadf449b0cff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "958de08d-7800-4cc5-9389-8e8ae9f97740",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "774ac6ab-6c95-4267-9d98-3ce8de842b2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "dbdb3ea6-6086-4791-a814-87abbf11c5f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "774ac6ab-6c95-4267-9d98-3ce8de842b2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "063dfb61-2f13-4632-a234-1637e3d62b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "774ac6ab-6c95-4267-9d98-3ce8de842b2f",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "b8264ab4-9072-4572-8ace-8b85867f118b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "a2ef795b-4045-4292-9f5f-27af4018d085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8264ab4-9072-4572-8ace-8b85867f118b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15437ebf-c56e-49f7-9af5-ad6afb4f2ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8264ab4-9072-4572-8ace-8b85867f118b",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "e404efcb-5103-4574-b61c-163cd95fa0e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "be091d3c-ce77-4874-a00d-b0eddd4a6001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e404efcb-5103-4574-b61c-163cd95fa0e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2212ef3-6dc3-4c0c-8574-1ed58e1f8e73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e404efcb-5103-4574-b61c-163cd95fa0e1",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "e3dc36df-f3ae-4e2b-a654-38991766cbbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "2c3547de-a8c7-47ef-8159-9947d248d37c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3dc36df-f3ae-4e2b-a654-38991766cbbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef0f16eb-52c2-452c-a12e-0cedcbe3ec9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3dc36df-f3ae-4e2b-a654-38991766cbbc",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "517033ef-7b14-4fab-8c7f-d25fdf427ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "15f6ea4b-6de1-42c9-b03b-45f4e8105594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "517033ef-7b14-4fab-8c7f-d25fdf427ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a11f1f3a-e9e7-4379-ae47-c1a80b051051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "517033ef-7b14-4fab-8c7f-d25fdf427ac3",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "c68bc6c8-a136-443a-851a-1686507ed98c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "f1496937-5010-418b-ac31-60f87b1cea77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c68bc6c8-a136-443a-851a-1686507ed98c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73b7b2bf-6b6b-4ada-8b1a-aa461a696fdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c68bc6c8-a136-443a-851a-1686507ed98c",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "3ee44b24-f7cd-472a-bd6d-828ad470eca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "c2e2906a-c92a-4944-a278-7e427ad5b2fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee44b24-f7cd-472a-bd6d-828ad470eca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bff1db28-4b63-46d1-8b54-ff65aa034aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee44b24-f7cd-472a-bd6d-828ad470eca3",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "88696bfb-98b5-438a-835d-61de61195067",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "ffb1ff4c-c46c-4d06-bb85-3123045aa024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88696bfb-98b5-438a-835d-61de61195067",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b218102a-dc9f-425e-802b-573e146236d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88696bfb-98b5-438a-835d-61de61195067",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "403250ce-453c-448f-afa0-7ff03da91b13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "2cebee7b-7159-4c73-8ff6-b86f1de36428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "403250ce-453c-448f-afa0-7ff03da91b13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adf8d690-3c70-474a-be08-2bd7c75cf862",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "403250ce-453c-448f-afa0-7ff03da91b13",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "d7e80d88-d7c0-430c-96b3-f7085f11459b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "0b597257-6738-4a43-84d9-048df3f2bbd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7e80d88-d7c0-430c-96b3-f7085f11459b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4db2c0d-05dd-4c8f-9c49-19c11a782473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7e80d88-d7c0-430c-96b3-f7085f11459b",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "60013bae-d749-48f4-8692-51f1064b11f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "c7d9112c-2fc0-4a14-b049-9ff0ab53a76f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60013bae-d749-48f4-8692-51f1064b11f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "435f02f0-c4d8-40c7-88a8-d5d5a2eb0d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60013bae-d749-48f4-8692-51f1064b11f3",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "a7609b50-0bf7-46a7-a6dd-f0612da6cf91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "c2234b6f-2f05-4701-8f71-c67e07350f23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7609b50-0bf7-46a7-a6dd-f0612da6cf91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faab849f-0ead-49ba-85b0-b7f725a0cf2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7609b50-0bf7-46a7-a6dd-f0612da6cf91",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "2bbb81f6-924b-4001-8ddb-e3a97b8b6e3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "4b48dc7c-4b4c-4815-a4cf-cb48023b68eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bbb81f6-924b-4001-8ddb-e3a97b8b6e3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "894eab83-6421-41e5-9ba0-827d50730194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bbb81f6-924b-4001-8ddb-e3a97b8b6e3f",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        },
        {
            "id": "fdc56ce1-5906-4530-b420-f67d2d0c1f17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "compositeImage": {
                "id": "4dd0883e-2ddb-468a-b807-3c4a1b50ccc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdc56ce1-5906-4530-b420-f67d2d0c1f17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f82489e-e12e-4ac2-b920-dccfd610a55d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdc56ce1-5906-4530-b420-f67d2d0c1f17",
                    "LayerId": "4e7112a8-56fe-40c6-8137-a706b1c9956b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4e7112a8-56fe-40c6-8137-a706b1c9956b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b53982f-2f60-4c77-9134-519e570ddbee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}