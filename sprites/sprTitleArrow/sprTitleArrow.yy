{
    "id": "f27d27bd-12c4-4d16-a518-4faeafb13670",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleArrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e945be4f-7463-4f53-b2db-31280b086729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27d27bd-12c4-4d16-a518-4faeafb13670",
            "compositeImage": {
                "id": "9ed481f3-18c5-47d4-bc3f-68bb55bdf250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e945be4f-7463-4f53-b2db-31280b086729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46d20c45-e55d-4eea-9748-565889938925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e945be4f-7463-4f53-b2db-31280b086729",
                    "LayerId": "5f8d22c7-e032-416f-8c23-8b5eeb8436b0"
                }
            ]
        },
        {
            "id": "9409c813-e951-4e49-ab69-cbfcfa93f502",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27d27bd-12c4-4d16-a518-4faeafb13670",
            "compositeImage": {
                "id": "96d4087f-9a78-4afb-932d-c7257841ed91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9409c813-e951-4e49-ab69-cbfcfa93f502",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37b22486-e577-4e06-9301-929266edf571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9409c813-e951-4e49-ab69-cbfcfa93f502",
                    "LayerId": "5f8d22c7-e032-416f-8c23-8b5eeb8436b0"
                }
            ]
        },
        {
            "id": "466c3219-ae5e-48c5-8322-641968a85c56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27d27bd-12c4-4d16-a518-4faeafb13670",
            "compositeImage": {
                "id": "29a0ed20-5d93-4065-8bc7-e44d1230fa2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "466c3219-ae5e-48c5-8322-641968a85c56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21819320-8b0e-4678-961c-5013693612dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "466c3219-ae5e-48c5-8322-641968a85c56",
                    "LayerId": "5f8d22c7-e032-416f-8c23-8b5eeb8436b0"
                }
            ]
        },
        {
            "id": "2df877f9-4c98-4d19-902c-eccd32741320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27d27bd-12c4-4d16-a518-4faeafb13670",
            "compositeImage": {
                "id": "8458daa0-96ee-43e0-a2fd-522bb67586c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2df877f9-4c98-4d19-902c-eccd32741320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f1b7155-3b18-44b0-8728-81b329a71519",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2df877f9-4c98-4d19-902c-eccd32741320",
                    "LayerId": "5f8d22c7-e032-416f-8c23-8b5eeb8436b0"
                }
            ]
        },
        {
            "id": "d3a63678-86ef-4f9c-8dab-4deef3f53115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27d27bd-12c4-4d16-a518-4faeafb13670",
            "compositeImage": {
                "id": "504905d8-14d3-47a1-8205-b4c678784dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3a63678-86ef-4f9c-8dab-4deef3f53115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce90d1b-4dea-4c38-adb8-29b1392f94df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3a63678-86ef-4f9c-8dab-4deef3f53115",
                    "LayerId": "5f8d22c7-e032-416f-8c23-8b5eeb8436b0"
                }
            ]
        },
        {
            "id": "775ce7a9-1439-41f2-b117-5b18567ef2d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f27d27bd-12c4-4d16-a518-4faeafb13670",
            "compositeImage": {
                "id": "2440a063-d8b6-4990-8ba4-ac7ec384e242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "775ce7a9-1439-41f2-b117-5b18567ef2d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbcc9079-8f41-401f-9464-7be2d7bcedec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "775ce7a9-1439-41f2-b117-5b18567ef2d3",
                    "LayerId": "5f8d22c7-e032-416f-8c23-8b5eeb8436b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5f8d22c7-e032-416f-8c23-8b5eeb8436b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f27d27bd-12c4-4d16-a518-4faeafb13670",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}