{
    "id": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuBlockA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49b503f2-0740-4663-a7d3-715a9423649b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "5dcef987-5e63-446e-8854-52b0faa2a8b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b503f2-0740-4663-a7d3-715a9423649b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2035027-3a80-4710-8ea5-c60e1bdb44db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b503f2-0740-4663-a7d3-715a9423649b",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "b19890c7-539b-44c7-b989-72bc2873a467",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "d1f7d5ab-e3df-43d3-a9be-5374998067c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b19890c7-539b-44c7-b989-72bc2873a467",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eb80763-9b14-485c-8cc6-f0c0122fcb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b19890c7-539b-44c7-b989-72bc2873a467",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "8565be28-7147-4a61-ab36-4d6a2ab0f285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "06263140-eecf-4c72-aead-a016250cf761",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8565be28-7147-4a61-ab36-4d6a2ab0f285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ed5bca-77b8-4c4f-b1a9-4b0d3aaea69e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8565be28-7147-4a61-ab36-4d6a2ab0f285",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "d595c854-601e-4885-9de4-1a9827062e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "5cd2128f-e249-471d-9802-4ae2723560be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d595c854-601e-4885-9de4-1a9827062e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e3ed38-e2ca-41d9-ab15-08cc126f7779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d595c854-601e-4885-9de4-1a9827062e3e",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "85839122-a369-4304-b103-2fa8c822f82d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "c7e7f034-6c38-4a84-bfc0-c2e115ddfa2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85839122-a369-4304-b103-2fa8c822f82d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac31486f-de7d-496f-b6c7-1e6ef61e91a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85839122-a369-4304-b103-2fa8c822f82d",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "b7f58b34-2254-453f-a329-588e2741db15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "270aab01-2247-4511-b2ad-5fbae1fdd158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f58b34-2254-453f-a329-588e2741db15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b578c7cb-fd3c-40cc-bbbe-4b5a7c793240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f58b34-2254-453f-a329-588e2741db15",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "2c43dbd2-5aa1-46cb-b686-1e55f7290ff5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "6ff60a7e-6b74-49f3-a9f0-eb5ceb9f0646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c43dbd2-5aa1-46cb-b686-1e55f7290ff5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a029e6-3404-4856-9215-9e1cc3167cd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c43dbd2-5aa1-46cb-b686-1e55f7290ff5",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "a4218612-f8ef-4761-a5c6-67617a95e439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "d6f9c4c4-3154-423a-8219-b153b4a3cb10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4218612-f8ef-4761-a5c6-67617a95e439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106bc346-dd8e-46fa-a30a-895892e15fdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4218612-f8ef-4761-a5c6-67617a95e439",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "24200896-9b60-4572-849f-3b3ba998d69e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "8e1e41cc-df2f-49f4-b730-45d304bfef9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24200896-9b60-4572-849f-3b3ba998d69e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951c4cb2-2ec1-4b0f-bcea-236c7d9dc497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24200896-9b60-4572-849f-3b3ba998d69e",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "61456246-db18-49d9-ad6e-9c7d8ff30cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "35973986-bf0c-45ac-a5f2-5a422582cb50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61456246-db18-49d9-ad6e-9c7d8ff30cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c03cbd-b2e4-437e-9e67-18d8b864bb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61456246-db18-49d9-ad6e-9c7d8ff30cca",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "f440e79a-8f34-4590-9b5b-dd1a2900b541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "e7f527eb-6940-4417-9a4f-ad7b7b68384b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f440e79a-8f34-4590-9b5b-dd1a2900b541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a09fa0-7932-4695-9941-440db17374dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f440e79a-8f34-4590-9b5b-dd1a2900b541",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "0702a04a-5d03-496b-b972-8be1a702aee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "5247a863-abf5-417d-a591-4bd48074ba7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0702a04a-5d03-496b-b972-8be1a702aee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f7026ca-649b-47b7-b5c9-10d9ab5fa50c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0702a04a-5d03-496b-b972-8be1a702aee7",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "c5f243c4-8df4-4d60-ae2a-da3e1eb32917",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "fbe7546d-f88b-4542-adfd-8095fc0b753e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f243c4-8df4-4d60-ae2a-da3e1eb32917",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec823a70-9877-4304-941e-c0b380e1e96f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f243c4-8df4-4d60-ae2a-da3e1eb32917",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "7f4f6000-15b6-4c4a-8cf5-e0b41d27ad1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "589ef61b-90af-4dc2-927d-c166d6aedcbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4f6000-15b6-4c4a-8cf5-e0b41d27ad1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9e4b937-484d-4445-8a7a-b478c378ced7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4f6000-15b6-4c4a-8cf5-e0b41d27ad1a",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "6e0640a1-9427-4fe3-862f-ca92cb40e7ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "ee8a30f7-a677-4a19-8c25-b68ee69d8eb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e0640a1-9427-4fe3-862f-ca92cb40e7ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "456abb15-f108-4ce4-a1be-01969519b61b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e0640a1-9427-4fe3-862f-ca92cb40e7ff",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        },
        {
            "id": "1257f4e5-2d6b-4945-90b1-947600584ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "compositeImage": {
                "id": "f40acab6-e58b-474f-8a74-d0852a94eeff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1257f4e5-2d6b-4945-90b1-947600584ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "335ebacf-e0e8-4689-9853-4d2fb56d12ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1257f4e5-2d6b-4945-90b1-947600584ed1",
                    "LayerId": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e54a2dd8-48d9-4a80-9a74-c39e60b15dee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aed7d98d-309e-4d1d-bb56-c5d76d1ad94b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}