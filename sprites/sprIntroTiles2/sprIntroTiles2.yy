{
    "id": "4bcd3db2-3ba6-4a4e-b980-9f5cc3a5c1c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroTiles2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23c67c93-a5c3-4c70-a931-7ff4adde7837",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bcd3db2-3ba6-4a4e-b980-9f5cc3a5c1c8",
            "compositeImage": {
                "id": "92e8c349-cac7-47fc-8478-8bfe1749c1f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23c67c93-a5c3-4c70-a931-7ff4adde7837",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f0518ce-d4e6-41ae-b003-4376e8832768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23c67c93-a5c3-4c70-a931-7ff4adde7837",
                    "LayerId": "8bf7281e-d2ef-4727-9c4c-ff398856f39f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8bf7281e-d2ef-4727-9c4c-ff398856f39f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bcd3db2-3ba6-4a4e-b980-9f5cc3a5c1c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}