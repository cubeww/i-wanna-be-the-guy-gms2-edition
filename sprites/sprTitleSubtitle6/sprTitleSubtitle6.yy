{
    "id": "6a621c11-4174-4858-a100-f8109809ad0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94c79e21-622e-426d-83c2-81e995e8acf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a621c11-4174-4858-a100-f8109809ad0b",
            "compositeImage": {
                "id": "600e5d6c-0617-431e-a587-09dbc47494eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94c79e21-622e-426d-83c2-81e995e8acf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d40d3c1-32e5-43fc-aea3-809a416f193f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94c79e21-622e-426d-83c2-81e995e8acf1",
                    "LayerId": "12a8173b-b7bb-4627-8ff9-5cc6d864657f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 157,
    "layers": [
        {
            "id": "12a8173b-b7bb-4627-8ff9-5cc6d864657f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a621c11-4174-4858-a100-f8109809ad0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}