{
    "id": "df224d7f-97d9-4b9a-aa72-aef65af56cbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b849a034-7f59-46b4-909e-b5f387aadf74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "df224d7f-97d9-4b9a-aa72-aef65af56cbc",
            "compositeImage": {
                "id": "423975d7-9215-43e2-bb23-cd8160e36e44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b849a034-7f59-46b4-909e-b5f387aadf74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43e54f47-fb87-4b22-a4c0-854f67288147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b849a034-7f59-46b4-909e-b5f387aadf74",
                    "LayerId": "2e9f20fb-cf80-484d-a852-63a710de6107"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 157,
    "layers": [
        {
            "id": "2e9f20fb-cf80-484d-a852-63a710de6107",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "df224d7f-97d9-4b9a-aa72-aef65af56cbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}