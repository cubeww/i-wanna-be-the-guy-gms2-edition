{
    "id": "1f64b2d2-5f50-400c-b5ef-a2aa9757d434",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 308,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62547aaf-09a3-4d31-9a9b-7a66847cb4eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f64b2d2-5f50-400c-b5ef-a2aa9757d434",
            "compositeImage": {
                "id": "6a1f6de8-9500-482d-b327-f2c41e66b79c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62547aaf-09a3-4d31-9a9b-7a66847cb4eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf18bf78-8e3c-42c9-bd3f-f279488d0104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62547aaf-09a3-4d31-9a9b-7a66847cb4eb",
                    "LayerId": "1fbb36bb-f8ef-4c78-935c-d103f3c64ee3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 309,
    "layers": [
        {
            "id": "1fbb36bb-f8ef-4c78-935c-d103f3c64ee3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f64b2d2-5f50-400c-b5ef-a2aa9757d434",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}