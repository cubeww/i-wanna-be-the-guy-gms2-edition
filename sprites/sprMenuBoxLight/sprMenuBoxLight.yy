{
    "id": "74d8eff2-cbb2-4eda-8741-90ab5682d7ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuBoxLight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 351,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b74dcf74-434c-4417-b645-ce03b040deb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74d8eff2-cbb2-4eda-8741-90ab5682d7ed",
            "compositeImage": {
                "id": "c2c345e8-8119-4609-b0cb-9710c1313825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b74dcf74-434c-4417-b645-ce03b040deb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bebdfa1f-5201-4195-9582-1d964f9dccca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74dcf74-434c-4417-b645-ce03b040deb9",
                    "LayerId": "5c8c38c2-e20b-4433-a379-3db3c2b43d36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 352,
    "layers": [
        {
            "id": "5c8c38c2-e20b-4433-a379-3db3c2b43d36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74d8eff2-cbb2-4eda-8741-90ab5682d7ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}