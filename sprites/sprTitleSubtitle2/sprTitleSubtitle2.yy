{
    "id": "72e8d10d-56e1-4db9-82c8-68a244d69f39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f91e9bb-3ae5-4dc3-bc77-77a430f312b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72e8d10d-56e1-4db9-82c8-68a244d69f39",
            "compositeImage": {
                "id": "14a4597d-071d-43ce-9c0a-a1a7b835b778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f91e9bb-3ae5-4dc3-bc77-77a430f312b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca34a5d-6b4c-4e48-b656-8fd599194a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f91e9bb-3ae5-4dc3-bc77-77a430f312b0",
                    "LayerId": "3030ba48-b951-4206-b355-6b06a74b959b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "3030ba48-b951-4206-b355-6b06a74b959b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72e8d10d-56e1-4db9-82c8-68a244d69f39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}