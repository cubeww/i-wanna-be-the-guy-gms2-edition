{
    "id": "bee126cf-a63c-4832-abf9-6f474c0eba47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 205,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22044855-894c-4d9b-b6db-2eefe17c2fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bee126cf-a63c-4832-abf9-6f474c0eba47",
            "compositeImage": {
                "id": "be5f214a-2067-431f-ac2f-6716a62ee148",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22044855-894c-4d9b-b6db-2eefe17c2fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b6cc7bc-77b7-4bfd-8adf-b7ca2ad75b7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22044855-894c-4d9b-b6db-2eefe17c2fca",
                    "LayerId": "a716e96b-ceb9-4de3-804f-e1f3c8364d6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 206,
    "layers": [
        {
            "id": "a716e96b-ceb9-4de3-804f-e1f3c8364d6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bee126cf-a63c-4832-abf9-6f474c0eba47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}