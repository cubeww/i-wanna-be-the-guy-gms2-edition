{
    "id": "70ba128f-9d0d-4db0-9f69-ee6c555bbbb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTitleSubtitle5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6cbc98f-8281-4850-8413-4b299bee939f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ba128f-9d0d-4db0-9f69-ee6c555bbbb0",
            "compositeImage": {
                "id": "1f6fd849-0705-493f-be53-f09477f216c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6cbc98f-8281-4850-8413-4b299bee939f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12143ab5-c7a0-4ac8-a73c-f9338ae5e33d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6cbc98f-8281-4850-8413-4b299bee939f",
                    "LayerId": "62add2de-1cbe-4f62-b4d7-a85445dbfbb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 157,
    "layers": [
        {
            "id": "62add2de-1cbe-4f62-b4d7-a85445dbfbb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70ba128f-9d0d-4db0-9f69-ee6c555bbbb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}