{
    "id": "cdaa2c93-6adf-48cf-b838-6c24559156a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBlack32",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98613a13-b845-4982-b9d4-e9baacb290ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cdaa2c93-6adf-48cf-b838-6c24559156a1",
            "compositeImage": {
                "id": "c26b70d5-af27-4048-87e3-1aec0e06fe1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98613a13-b845-4982-b9d4-e9baacb290ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b476b55-dad5-42ce-938b-3c197c8677ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98613a13-b845-4982-b9d4-e9baacb290ed",
                    "LayerId": "ce94d8a8-fa0f-402a-b003-af94d7801cb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce94d8a8-fa0f-402a-b003-af94d7801cb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cdaa2c93-6adf-48cf-b838-6c24559156a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}