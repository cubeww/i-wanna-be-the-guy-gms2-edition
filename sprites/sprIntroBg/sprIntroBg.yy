{
    "id": "760ac079-c45f-4dea-a6e8-6b1372333bec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroBg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 750,
    "bbox_left": 0,
    "bbox_right": 480,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a094518e-4011-4ce1-87a2-934b5a68e39b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "760ac079-c45f-4dea-a6e8-6b1372333bec",
            "compositeImage": {
                "id": "5f6f3731-6367-4a30-a9bc-fca9291ee595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a094518e-4011-4ce1-87a2-934b5a68e39b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52227e35-9e5e-42c1-b4a2-d07cf1ad74be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a094518e-4011-4ce1-87a2-934b5a68e39b",
                    "LayerId": "4c8af56a-dbf3-43a1-9291-e1f63463378f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 751,
    "layers": [
        {
            "id": "4c8af56a-dbf3-43a1-9291-e1f63463378f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "760ac079-c45f-4dea-a6e8-6b1372333bec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 481,
    "xorig": 0,
    "yorig": 0
}