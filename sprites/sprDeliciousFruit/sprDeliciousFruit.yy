{
    "id": "c723e1c8-2977-4d7c-8829-c35fadc34a7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDeliciousFruit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 21,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b59dda71-7ba5-4284-88a8-44722476502f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c723e1c8-2977-4d7c-8829-c35fadc34a7a",
            "compositeImage": {
                "id": "0f4df65f-083c-4d73-9d2e-97a6caa54df0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b59dda71-7ba5-4284-88a8-44722476502f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d76741e-e86d-40d1-82d2-ae456e1c1530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b59dda71-7ba5-4284-88a8-44722476502f",
                    "LayerId": "0d9fc1fc-5463-421e-814b-4194989c6c25"
                }
            ]
        },
        {
            "id": "b4cf0b46-a327-40b5-9154-0ba7477fcbab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c723e1c8-2977-4d7c-8829-c35fadc34a7a",
            "compositeImage": {
                "id": "3ddec332-73d8-4e2f-a993-48a124a8ce7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4cf0b46-a327-40b5-9154-0ba7477fcbab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0776e88a-9bce-47bb-928a-cf1741faabb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4cf0b46-a327-40b5-9154-0ba7477fcbab",
                    "LayerId": "0d9fc1fc-5463-421e-814b-4194989c6c25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "0d9fc1fc-5463-421e-814b-4194989c6c25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c723e1c8-2977-4d7c-8829-c35fadc34a7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 0,
    "yorig": 0
}