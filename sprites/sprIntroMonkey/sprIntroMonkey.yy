{
    "id": "e34840af-6adb-406a-ba55-a20752e13714",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprIntroMonkey",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 6,
    "bbox_right": 145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "173ef18d-ef6e-4fdf-8a02-a9a519d39522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "9a1383ea-3594-4b1f-ba9e-712d6629ed19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "173ef18d-ef6e-4fdf-8a02-a9a519d39522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f92d32a-e022-4c26-a841-bcf0bb287b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "173ef18d-ef6e-4fdf-8a02-a9a519d39522",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "85819d0a-9854-48fb-9b91-5dc84ee9aa75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "c8dc51a6-d43a-46fc-a338-2997c9d1f43c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85819d0a-9854-48fb-9b91-5dc84ee9aa75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af9bc093-d90f-4c30-93f7-ec9f30d05d89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85819d0a-9854-48fb-9b91-5dc84ee9aa75",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "fa972d84-7300-4249-8719-170cd4232fab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "3933d1ff-25b3-4708-a839-02b450eb067d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa972d84-7300-4249-8719-170cd4232fab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6de0279-a783-4dbd-b91a-7fb044ddb67e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa972d84-7300-4249-8719-170cd4232fab",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "81a4847b-9f95-4b46-b06f-2e465985bb35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "15a994ff-f745-4697-99a5-98fbd9867b2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81a4847b-9f95-4b46-b06f-2e465985bb35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "320bba06-3418-42cf-87db-e597f90042b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81a4847b-9f95-4b46-b06f-2e465985bb35",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "a3234f32-3658-4f22-994c-eada1f619a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "db904ef9-a72c-4e82-b74e-f6f09e50c175",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3234f32-3658-4f22-994c-eada1f619a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e1f65fb-ad86-41b6-8051-a117c6770b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3234f32-3658-4f22-994c-eada1f619a86",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "612bd7ae-c296-4a04-835f-ad76d5130809",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "be3fefe6-15aa-4c39-95a5-f4226981c0d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "612bd7ae-c296-4a04-835f-ad76d5130809",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1003e7-6ad5-4b2e-864c-506cadc2115c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "612bd7ae-c296-4a04-835f-ad76d5130809",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "2c218706-9a56-433a-999e-8670217a2f03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "f8cfdbc0-0574-4c73-92dd-f123a83a3aa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c218706-9a56-433a-999e-8670217a2f03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c64c4f2-a330-48e2-bac8-1cc4ff111268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c218706-9a56-433a-999e-8670217a2f03",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "3dc2abcd-35bc-4a73-8605-f85f8acb71cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "55acadac-d624-4e3d-a2b9-815400db2b78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dc2abcd-35bc-4a73-8605-f85f8acb71cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0de5bcb-cca2-46e5-9404-10ea404a48d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dc2abcd-35bc-4a73-8605-f85f8acb71cd",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "55059020-304f-49a6-8988-3990548d2d4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "cd65ea56-0a3b-4518-96b5-e028d98a38f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55059020-304f-49a6-8988-3990548d2d4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe4dada-dd43-47b4-83bb-758bdd220e3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55059020-304f-49a6-8988-3990548d2d4a",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "579e4f74-692a-4166-a620-92b2578e195e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "b2c9f60d-c2f9-4f77-ad7c-af3c8e53a3ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579e4f74-692a-4166-a620-92b2578e195e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c72806e-302b-435f-b39c-984beaec000c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579e4f74-692a-4166-a620-92b2578e195e",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "b57b1cc3-1310-4efa-8b87-ffffad25cc9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "d8f4ea99-c713-4631-845b-72cdb400d59b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b57b1cc3-1310-4efa-8b87-ffffad25cc9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd799d59-8176-46b5-9b13-03e4d2315be3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b57b1cc3-1310-4efa-8b87-ffffad25cc9b",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        },
        {
            "id": "cc4b3eec-0a6a-4dce-acff-0de4f7d0ffc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "compositeImage": {
                "id": "d8abc602-ffc9-40de-ba43-51eab6037b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc4b3eec-0a6a-4dce-acff-0de4f7d0ffc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37b1d6f0-7eae-4e93-8189-fab3c41a2503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc4b3eec-0a6a-4dce-acff-0de4f7d0ffc4",
                    "LayerId": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "16ebd8e3-18ab-4a27-90b2-83cebd1c256a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e34840af-6adb-406a-ba55-a20752e13714",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 146,
    "xorig": 0,
    "yorig": 0
}