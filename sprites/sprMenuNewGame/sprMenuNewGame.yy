{
    "id": "6ce2521e-4575-4a16-a4fe-da18917783f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuNewGame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "295525fd-f815-4852-b101-84f40a58d7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ce2521e-4575-4a16-a4fe-da18917783f1",
            "compositeImage": {
                "id": "922b65bc-8be6-48ab-8cac-f13728d5b1d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "295525fd-f815-4852-b101-84f40a58d7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fc33a47-1c6a-4e1e-9941-38562714dc79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "295525fd-f815-4852-b101-84f40a58d7e6",
                    "LayerId": "c0a83200-63e9-4347-b7ea-e5b27304beae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c0a83200-63e9-4347-b7ea-e5b27304beae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ce2521e-4575-4a16-a4fe-da18917783f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}