{
    "id": "3d6375c8-0f87-4858-98f0-8803a94be2e9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuArial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1b68c0db-1b7a-4492-b446-e8381c41162d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a0147386-455d-4c11-b756-5071bfb19eb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 123,
                "y": 36
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "eff67ef2-14fb-4de9-b1ec-baa6449ae89a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c8c56a59-64f1-4c76-b163-2bdebeac77ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f4e0d365-674d-436c-a43e-edbcc5fdb653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "670d02ce-7f79-4ef3-8398-b593c1953caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 83,
                "y": 36
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "acc9fa40-ef08-4703-a501-ec0a237e8dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 71,
                "y": 36
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3c54a4fc-c202-4119-9482-753d715a6523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 66,
                "y": 36
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4c36c4f3-8fa2-462d-8c10-60d641d16680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b6c5243a-e918-4239-81d7-98ebf4c279e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 36
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ab47960c-877c-4d43-a7f4-d36b3e5d90db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 128,
                "y": 36
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "421fbaff-d987-414f-8e90-958cee5491c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 36
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e885d250-9c44-4086-a9e5-2c521dbae545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 30,
                "y": 36
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e15c9e49-0015-479a-bf8c-a5a3a42aadc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 36
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fa8b9c80-dc50-4851-a0c2-f803c1d2a10b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a5e29382-50f2-448d-a246-dc1a197b15e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1d8de221-6101-44b5-82a6-a7916dde2929",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1c4a91e9-d501-4e27-bf8b-76f869e78e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 240,
                "y": 19
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e6fe1649-c775-451f-885e-7b259f044719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 231,
                "y": 19
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6ea6437a-45e0-46c8-acbe-e6eb0ee1c997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 222,
                "y": 19
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "557a4e86-6617-42f1-856e-80ef84cb2970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 213,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8c4143ae-4b71-40b0-ba4d-0f69c07898ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0960880d-c065-49ef-be22-2abd511c54a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0294fd90-de18-4856-9d12-bb51eddf011e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 144,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a780b798-aa7d-41f9-b550-7ddcab6f2fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 153,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a44047c2-b4fe-413c-a87c-f686c330b141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6d747801-65e8-41f9-a570-fd69ae22a5ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2efbc146-af38-4fd6-9517-9f12165553d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b5d34241-c830-40a8-b356-dd20f334a34b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bae5d811-dc39-4105-9756-b3596da88b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 53
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5c5cac6e-2604-416e-b235-75831c65e226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 53
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "76b7889e-0b9f-484e-bb24-33ca1fd66d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2e6a1f4e-f7f0-4887-be95-5c34f7e4a481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0298271f-d88e-40ca-821f-4d573959191a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 35,
                "y": 53
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "76ed067c-d5c5-4c24-8464-c4921d33773a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 53
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a0fbcc19-0e89-42ed-85ba-e64f3f17681c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c1d641a1-c2a6-4f64-805b-211c467a1f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c60ed07b-d0a0-4265-8692-80efe9532fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 242,
                "y": 36
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1d9773df-fbf4-47ff-aee0-cc4f1dcc78e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 36
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b1301a6e-f9de-41ac-8711-d22a01e7d982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e64e3b1a-b60f-4d0b-aef1-911a144a8215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 209,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5e909c5e-5510-4eb7-b0c7-f7a77588118a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 36
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7d00cdb8-3204-49e3-a35b-55dcc96497a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 195,
                "y": 36
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ec326c58-93b7-4628-aeb5-359c6fcdd76f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 183,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7e909225-659b-45ed-a4c0-e3795aa4247e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 174,
                "y": 36
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d7b76c7b-49bc-4e6a-864d-88b007b70194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 162,
                "y": 36
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "25c47824-ba96-42e0-ab9b-e0a1110317f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "427e44a9-3496-4a27-b602-006e09c1ee24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 19
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f6f29fca-109b-433b-8930-c50daed80f30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "4450a884-1d94-45dd-9280-6929f62306bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "647b21b2-afea-42c8-80c7-31f3432d2bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "963e61ba-3099-40e4-8bf1-656759bbf358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0996c786-93cc-4e9a-a7f9-03a8c29ece76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ba7dfc5c-0b18-4376-a051-1c9f76e3c458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bd506523-5c1a-4ad0-a0db-a328df6c71c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "12bd2285-38b2-47da-8a0f-4c7e85556c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "de275df4-16a6-4b09-8f3b-892f53b29e0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "eb8f3118-4791-4467-8570-0c3fc025be17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9d3ed1d2-5944-4669-9611-0ab8a91a8a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f0ea9d98-b537-47ee-8749-e956dae1968e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3f152d4e-5fd0-479f-baad-b8a92d95ceac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9fe748e0-414c-4276-ad26-9771ed5fa441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "344fee9c-0fce-475b-bac9-9d4fbdb12e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "52d97df5-ab82-45e8-96d7-7a6cff98ed1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8dc08779-507e-4929-95de-eb5019c960b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c841c1da-713d-422f-8789-77a5cc0cb97b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d5fe6f3b-e62b-4e82-a622-a673012b1b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "25c700ba-9fc4-44d8-9c46-e40444c98116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d1ba8038-ee6d-4dc3-99b8-fdcc96d8b775",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "aa4a2964-db79-4b98-87fa-511290c6c289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "089c9df6-9f41-44e8-80a1-3766009012e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "18892eeb-bf4d-44e1-9c9e-7b193a7dc0b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1eaf3d62-a156-4737-a769-4e13e71185f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4d5aadc1-2f4e-445b-a77d-612cfb95afb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9604bdb3-63cd-4097-b8dd-8faab81a4eb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 164,
                "y": 19
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "347a4925-e8a9-411a-af3e-b84850aba1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 154,
                "y": 19
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6344090e-ef25-4e57-821e-8b593455516b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 149,
                "y": 19
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a5d9bd46-c3bf-4815-83a9-f47c55632034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 136,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d5f84763-c5b3-4064-a6dc-db4ac2fb01cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 126,
                "y": 19
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "85cab949-910c-4195-ae5f-4143d2943a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "fbdbb709-f944-4a13-a331-1f6d5f5c1024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d872fe39-15c7-4810-b06f-c1587ec354e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4db34b7d-af23-4d20-a43e-37d73d3b7d66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 88,
                "y": 19
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "561d41eb-ef41-4758-8250-61379ff3af34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 19
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "652b0018-9ef0-478f-a52e-ec3445c720bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 81,
                "y": 19
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "70b2be55-d283-4bfd-b5d5-b486563c8e56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "659787ff-51fa-4f25-8383-6b2b5f30f00f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 51,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e447526a-9b4b-48d5-b9c7-366c9d4dc051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4235515c-f07e-44ff-a71e-b446c57360a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fdc6d6e2-cdaa-4fe2-8f49-0ff3bb89b056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 18,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "48bbc139-6610-468b-a13d-0434168aa4e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "be77c122-70ba-4182-9058-2ea3216868b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b7926186-ef4d-47f5-9125-c28a8a5b8e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3c05c4d0-70de-498d-a727-48a1c9b73c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "93069e90-db41-42fa-a99b-0f84faa85945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 119,
                "y": 53
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ccd8becb-21e3-4669-8490-3d0036a715bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 129,
                "y": 53
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b754485b-8576-4595-b7d7-0c7eb6224d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "16fc7cca-1be7-4577-bba4-d76d3eff8df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "9bb30d55-f374-41e6-98ac-384602f8bea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "008b37b1-f6e8-4595-b166-acad8abf661e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "5f6d5f99-38e8-4008-99af-0a449252198f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "c70e5a7e-ac11-44ea-8699-847c88c38410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "e9f47901-a142-4938-8fd1-7d0a08432417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "dfe1c047-6105-4dce-acd6-f7f2ed1f47bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "a57a7f7a-5866-4fc8-9550-67ed319bc744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "e61f9361-9d73-483b-a5bf-105d1e4406e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "fecf8135-b915-4db3-9c7e-d03ebf5fe1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "b9fc65d7-5345-4f69-81f4-8fb3fb196072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "2e0a3f66-21e9-4bd4-8080-a9a1ac9806df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "196770da-7866-456a-91d5-26166d93b484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "491c0067-b059-48f3-af27-3f3320c337d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "443d40bb-c2d6-4dba-8b9c-b1026c286569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "3526a94d-714e-4fd9-9306-77542a746cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "2fb8728c-249e-4d54-996a-23d0137178a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "6f7f1ef2-39ea-4c5a-976f-b31eb1b8b205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "17622d20-53dd-4aaa-97b8-e72e772f9630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "14b92250-73b2-486f-92b8-8e2b1839c01c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "f8d23017-10ef-4946-add2-8224a8aac622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "dccb753d-b838-42e4-a28a-eb3d6032809a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "33b5a252-3de3-4843-af6a-3113fefbbab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "bf4fd5c5-04c1-418b-b7d3-4352dcd0d8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "3ce4bd0c-6ee1-429d-9989-bd306d025c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "6365b3c4-a62d-429f-8028-2df98e137d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "16b6a2c9-23a5-4ddc-b155-6e6abeae5fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "dfe89e7e-15e4-41a6-aec0-504e6d0b5829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "42f0d0ac-4c9a-47db-8b14-9f63a58e8a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "086009ea-7cb2-41ac-be64-385206924868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "36d46ceb-5aba-42e1-bf41-2a672f55a93e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "5ff05819-607d-4ecd-81a7-c80b9f4760f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "0f79d94d-c050-4583-8d46-dde992550804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "b3256c86-8f62-40d1-95e8-2b97910f57c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "58a8057c-693c-4411-8731-ccf844edbaba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "cd04ffc0-96b9-4fcf-86dc-912e16c11e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "2988b090-af7c-4db6-b332-9b813e4acb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "700be404-0239-4a89-a048-3cbb89099633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "8056c98f-c61a-44ad-9846-9bb8159dd9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "8e4c0b6c-fe21-46fe-9eca-e059bb6b3159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}