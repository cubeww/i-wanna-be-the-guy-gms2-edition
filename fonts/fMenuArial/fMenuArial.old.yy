{
    "id": "3d6375c8-0f87-4858-98f0-8803a94be2e9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuArial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1140741b-23db-4987-b4ec-4279f39100ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b2c654f1-d6b5-4c9a-8471-32dbec490395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 135,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "89942434-29b5-4240-8b4d-bba627714330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 128,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d7d5b746-01f1-4865-afb3-03e7f6b2f4f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 117,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a088f7ed-8b94-4765-bae5-bf710edf2997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 107,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8b4c4e59-2c06-4e33-9134-39bf0756cbb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 92,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1b07181d-a519-470f-823c-adb83c0a1343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 80,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "918423e0-922c-43d6-a675-527e48267702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 75,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b0522c13-f016-4fcf-b5c2-2aa6f9e167c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 68,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "00d886f7-2e89-45b7-96aa-a9fced5d7567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 61,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "be04214f-b524-40ec-b3b9-5e0fba5144de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 139,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1f805c87-e2c4-4c8e-97dd-a4e3790d915e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3b001a86-29bb-4357-8937-f6216e22c715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 37,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9a1bbb47-5622-4a33-bc19-139a1a761af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "57dfd25b-d256-4ca7-9bca-18a1b96bd350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 26,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ff49ebf0-5961-47c2-bfdd-767b2069fa7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 19,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "784ea471-3e4b-4e6b-8936-557cd017b0dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 9,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a3787198-28dc-42e2-b64d-22a130581dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "32f4ed17-fd25-4955-8d5e-f358e4c50ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 239,
                "y": 21
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9e1cf4dd-c29c-4154-80d7-9b9d7c46915b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 229,
                "y": 21
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ae94bbc7-e05d-44c5-a3ec-c52c25906e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 219,
                "y": 21
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "97f6cc49-de55-4308-8b25-4c95ecdacd1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 41,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d99767fd-7c5c-40b6-9164-17a3766d9dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 147,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5519af39-20ae-4cf3-8384-0b42370aa770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "03797903-66da-4877-99c6-42bb4418e9d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 167,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "df616c73-30b2-4175-92aa-b1c3ee09ffee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 138,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1b69d01d-a968-4dae-9f25-8873b0cd9970",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 134,
                "y": 59
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "269dcd39-b7aa-443a-a618-6b9ab751b87c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 130,
                "y": 59
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ae4eeffc-88ce-4690-ade9-8ed9b3ce584f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6351081f-f628-4b63-aa43-ff68209db67f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 110,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bd4ac523-c5e3-4d3d-b4e9-c65a8e070cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "43c11868-da43-42aa-8cf3-28d1a7b59e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 90,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0c0491aa-9c54-4955-859e-ffdec0815d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 73,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7e484719-781e-416c-9c50-ea8318c41223",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 59,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "17caf825-2b6f-4570-aa49-83b00927b47e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e4c76ec0-6e41-4990-8d87-abc7ba06a571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 35,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "613278b7-805d-4bcb-af8a-3eca475e71d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 23,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7727e570-348e-44cc-ade0-7deeb4ea0a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c1200254-50d0-4320-970a-114f884fcf80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5c05b38b-e1cd-4486-9b4a-55831d80e084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 234,
                "y": 40
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "94049027-8bb2-4761-84bd-d4f186844506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 223,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5b3cddf7-d0a9-48c1-8fcc-1a8261e5b965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 219,
                "y": 40
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "380e47cd-182e-4b66-af8e-fb7eb2798dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 210,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a935dc11-efd0-422e-b749-19ae4e35875a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 199,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b38a2b1a-50fa-4ee5-9a6b-e0a6523109d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 190,
                "y": 40
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "583d5c02-c150-4e91-8d12-fffbfdbcf8f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 177,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "16a111f5-97f2-4b34-88ad-96930d2627e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 208,
                "y": 21
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "87aec4f2-dfc7-4c94-9857-5eea2861d176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 195,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5e14e520-7dfe-4498-8d95-5d58948ad28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 184,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "52117cb7-2f99-4627-bccd-85ae31804374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5d3682d7-2b94-4203-9895-8e8bd3dff1bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a2a29081-7b3b-4fd0-ad58-9fc2c1875914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3b8ab648-c0cf-4c69-b060-a5e193a124b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3db3a839-900c-4b02-a0ef-a6f0ea339c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "38eb5e1f-34d8-4512-84b2-8471c32ee8cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7168b057-73c3-499d-9182-8ddde33bfe87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7b8d9401-1dbb-4b7b-9c0d-f35d750e6438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "331b83f0-8a67-40dc-b236-1876cbe83675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "07cb3315-c282-4da5-9479-6e690722ec3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bc40ac27-e69a-4c90-93b7-343aab46edca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "41c52363-c3d4-4c5b-a297-2f63f11f221a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bb4e2d0a-de43-4b3c-b6f3-b9890280d08f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "38a056c1-d1cb-4a93-b65c-e7233d8cb375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "aadee010-c9cc-4db1-bfa1-f02896f5c6c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c96c26a8-a471-4962-a79c-f8ba5c1a8853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "df3a90ad-11dc-42f7-abf1-2b3f3237fed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2c5d39f8-472f-42ad-87b3-b5f1e52d1e5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "932697ef-fdde-491a-9532-57ca89f9183a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3a3d7e8c-aef6-47ba-bc2d-82c67112a912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "488ed5d1-c167-4766-a0aa-474f4f9e0a6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2e1943d3-b5bc-4b35-aa4e-432dac793f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "dcc171fe-d818-4ed8-9acd-13bb48562107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "faaac9b7-f633-4586-8841-cb9230162226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 76,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4b369735-da42-47dc-8af9-30aebfd98436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "58a33cb8-e662-4a28-b410-b6570ee71cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 169,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f8f64fbe-8dbe-479f-bfe9-748df3c51b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 160,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8ee583f2-e4d7-4bf0-885d-071ec95fd674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 155,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4ddb908b-191d-4e4b-9197-42801ef9be0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 141,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5418ff1a-becf-4726-9e9f-91f29556d49b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "cedc2e24-822e-411b-acbd-85197ac14b1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 121,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "26488c60-846f-420f-9153-e540afbd72bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 111,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "12582139-6463-4858-88ff-ef3bd0f64bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 101,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "dece0f64-ce2d-45f7-9d02-f19e2adc1301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 93,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1d21c818-8acc-4709-a6cf-1346be3c4d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 175,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c60ceb48-9576-4795-aca3-ee53b8b07798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 86,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f576f722-fddb-4874-b89f-f6749846146c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "205b825f-ac31-488b-a731-76a09c7e3f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 56,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f1ee2e49-f468-4818-8341-d94fcefbf09c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 43,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7dda2adf-c998-450d-b1db-668fb1d0e273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 33,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1592c458-3a82-4c73-8ea0-7f764ef7d7e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 23,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "463e3b0b-d80f-4aaa-8b0c-38d215bf6ca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 13,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eed96283-66e7-4a5f-a097-ce888d2a0528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 6,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "51abdd69-15c2-41b6-b907-1f6e8193ffa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bdde9045-0723-426f-b65a-09e0cbf57a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "051eee0c-82aa-4009-ac77-9ace479f692c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 148,
                "y": 59
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "799a2bba-547b-4736-9643-87f03bbc313e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 15,
                "w": 9,
                "x": 159,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "be7e8571-3bd0-40d8-afa4-3722059c86f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "ee9f2f86-df8a-4a7e-9d11-855b56beca95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "f395024d-3524-414f-a9c9-b48638732496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "75f2c9bd-c5c8-4705-af1e-465d4663f29c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "7176023c-2e20-4eba-bb17-061d6e141ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "49b1e07f-dd82-4cd0-8928-590017164a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "243292f5-c823-4676-ab05-87498742d51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "687396d0-4e37-4fd0-ad18-250690a7f00a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "89343634-1ee0-4c8c-9147-0ee6ccad3677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "b8189a78-91d2-41e3-92a9-4ca6fc57ecb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "072301a5-40f7-4994-a197-625e3f8c668a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "605a23a6-e0a3-4973-90eb-aa212a287178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "f55d48eb-6f2d-4d4a-86b3-8417c8c25ca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "7e357e4a-c71e-4fe6-a027-8d4f7651627e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "24738ae4-47d8-41da-aec0-bc90f6d889d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "f2a51f54-dc9a-44da-a083-9ee7d4b96746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "ea366822-868e-449d-895b-e714eee2d0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "84940093-709c-4ca9-a4c2-6995c86e1f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "c8ba110d-d4ec-46a0-9434-82d1cb928662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "729a1cc8-cd75-4926-9d61-15e88636a0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "e6e489ba-03fe-46fe-bf85-1ad2de689092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "8c8cae68-e8e2-4469-b345-850b61cdfdd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "2ad69125-22ba-4618-93b3-b88ea1f6479f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "2b5da2e5-fa24-4a99-bf72-15e60f3212f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "87ada2fe-5a5f-43e8-a35e-240c93353d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "6bc0e6ac-5658-4a8c-b9b6-05a35bf21b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "f66f0c5b-8c09-4060-ac16-53b07d81a3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "9c3c4960-0801-46c8-85b6-096b52a19ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "770de443-5ddc-4b6a-92f5-177128de9220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "70920fcb-a43e-449f-9370-11848d75dacb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "7537d4fb-959c-46c4-966d-713cfcab6137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "39941cfd-34f4-45b3-9277-bd853de53afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "a042d986-d0da-4fca-83f7-ae9cb2bc35f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "0d63595f-e889-497d-878f-eb08ef772326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "9f93ff40-ee26-4880-bbc2-1a3f993994d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "575709c3-0d42-4ee1-8566-e84a9ea87d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "fa4a2b59-e892-42bc-8992-20544632da53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "f71338e9-494e-4204-8747-bc4d9ee435c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "ecea06a0-1781-468e-a35d-38c98b7da9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "736c9d61-77a1-482f-8df0-5de5e09bcdc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "841e21ab-f335-47df-b7d9-cf4b38844da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "66f18c90-f862-4abd-bbb8-2712aaa2421d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "5b2e0691-5951-47e4-a8b8-27b9082a3023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "2280b775-a1ee-4ca0-9037-1608bcc62a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "52404129-a59e-443d-b11f-6f9b33005b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 11,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}