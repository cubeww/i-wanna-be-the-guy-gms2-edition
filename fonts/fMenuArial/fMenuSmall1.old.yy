{
    "id": "4f8607c6-4b5b-4d81-986d-23e43da44244",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuSmall",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "704ffacd-56c3-4d60-80b8-279d3f2eb993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8baa5785-7e2f-43ed-ac1d-20624bce11fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 142,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "39e6015a-59ad-45bd-bdd9-199795b1c158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 134,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ba6e0737-d677-400b-804a-99815b8125bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 120,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "91b863f8-bb59-4d1e-9f73-a0b8d24c5320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 109,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ff24493b-d33e-4a1f-ad15-a6f8bcb2e1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 96,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7b746596-69bc-4153-be21-7b10aa5106e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 85,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a1119a45-32bf-49b5-a5ff-1507282d739b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "68a62917-68e0-4908-bbd4-48d6693f637e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 73,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "337ff131-36af-464e-8412-374cceccc1ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "766c5114-6ee6-40e7-bd86-8375d68bd714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 147,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "87d7efa2-f312-4507-afcc-77c3219a85f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 57,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "34035591-ef85-4411-aef4-7da7f9a71d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f14ff09f-bd39-4a56-bfb3-8a4aa056eb88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "64aec59a-b1f4-47c8-be52-e6012d57ab6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 29,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2fd4805a-ec45-49e5-98aa-e0a80ebed6d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e9f49ff0-9989-4e32-b835-40cd61069287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 10,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "89a0eb5e-fcbc-497c-a26b-e9faa19d9e32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1b1c5553-0300-4d8f-9ee5-827a64ba9d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 239,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7475dead-0d15-40fb-a7e1-4bd781861d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 229,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9c78cbe6-e822-4751-bd86-27c3e3f9af0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 219,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ce650a3b-332a-4841-84b8-db2fc218e999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 47,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "89280937-810a-45cb-a0d7-571aa2c498de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 156,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a6979faa-ff9a-4840-b23c-3f821b6d61dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 166,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9e452ae2-e6e7-4fe2-969c-b96656aa29f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 176,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a4d58cef-d522-407f-b051-47b7faffa685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8f1d3436-db22-464d-87a0-d9fe2e4c772f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 126,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b8b5e8e1-4f13-41d8-96be-e520d56dea48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 121,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "58bfd7fa-8b83-4ec0-8404-7309f65a75ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 114,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "174c5c1e-d553-4c64-96db-19cde0c608b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 106,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f2826614-fdca-4544-b4e7-e3fb89f0a8c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 98,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "acb38e7b-d074-4187-8c23-9665833198cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 89,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9d6797cc-e5d7-470c-9311-5582ee048f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 75,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4467b8da-fc39-40c0-988f-b1c57ce95616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 64,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "44478cfb-54b5-47fa-8b06-dd0ec2b87e7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "42b871d6-a006-459c-b1d1-bf1c560637a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "56a41a65-58e3-41d5-9828-586c5ad51a9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 33,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "63f2b3d3-6ff3-4584-a3a8-fb7e6b504660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 23,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "38e246a9-707f-483a-b4e3-96c32489d8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 13,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "23b4fb82-a9b9-496f-a6ef-4b58700affa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b8a52fbd-a7d3-446a-b5d5-4398ca643c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "992396a4-a291-485e-a4e6-80b8679f1049",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ff014675-9529-418a-b024-11c923422bc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 220,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "76837fba-3d7a-4902-9696-0e22972b246e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 210,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e3b91f8b-9ded-4063-9c6e-b709bd8b1783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 200,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "42e14e5b-2f17-440d-883a-401a5274567c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 186,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "345ed2c9-be47-4469-9bed-b35a4859af5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 206,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1aa6511f-0adb-42c0-b092-8b9db626db65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 194,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "367183c3-f2f9-491c-b7d6-3e97a09a4e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 185,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cc9e12bf-c6e4-419d-a484-8fd63f075e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "64001e5c-e8ee-4efe-8b09-7779c5024098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "49459069-ab60-4f5c-834f-5fc46e170d47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "58ad994b-524b-45c7-b5f0-6c9cd450734a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5ddcdaf3-5f1d-42df-bcfb-660f425a72c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "172a24cf-281e-4a5d-b189-fca4f5f36635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "55c58b14-eb58-46b1-b3b9-21f9124c34e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "af4f0816-4eff-4ea6-90c8-6bfef73a4c6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5a963124-a114-4f8d-94cd-1908b256ba7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "65b8e429-bdd0-493c-b902-b456798858b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8cd0a4a4-aece-4920-8fec-2bd6932c968d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "504d45ed-ccd3-4606-83e6-f553ea10f218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "04b1da75-5248-4e66-a33e-0271e3bd4144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "cdcb0f66-a6b1-44f9-a2d2-54a25dd33d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bc7ee560-a9dd-4207-bd04-d38862bceaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a0595a84-ac2d-4b2f-bbab-416741197135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4200c3b7-c463-4000-8b98-4e226307fe87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "552a6429-9133-4e65-90f8-742e33d446ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9842a69f-bf68-44d3-994d-e606a89f0c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "236cabce-aa4e-4b2d-9b9f-e5c8ba6e67c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "82fbc1a2-6c8e-4e52-ba9c-ad68f7a60064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6896f9f6-4761-409c-b732-7d377f72853b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "8bed41e6-86c1-4823-b636-4f1f0901e90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e9863389-1d1a-4743-a95b-4fca7c0e64fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "9d06ae30-62c8-4ddd-944e-d7c0232569b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "899c5ef5-2fc8-47e5-92b0-be00d22c7f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 169,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f1170100-2b3e-4b4f-91b1-50bcd660c9bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 160,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1a23030d-3c99-4d16-a25b-76e3ee77b421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 155,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2345fdfe-9ae3-4231-93b0-bd16cdf44a21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 143,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bd48ab9d-1ac3-49d5-a0ac-ec1c207eaa2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d11746f8-8fb0-4936-b7cd-acadda435f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 125,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4b23c59f-eec4-4d91-bf62-e18866c7ce82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 116,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "724fecb6-aaa6-485c-abfd-754cc88e2a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 107,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "616ede91-051b-4cde-8ac1-49362b5257d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9533484e-8b76-42e6-8fdf-d68ca062a211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 177,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "602feb87-9396-4e8b-908d-19cee3c79842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 91,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a08f840a-a9a3-4bab-b932-ca62926d860c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 72,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "97d0ee0c-b2e4-4167-a5c1-7d54d725ba75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 63,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d44d7bba-bb1c-487e-8f7b-5d573a79ecf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 52,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "0bccdb5c-04ca-4659-a94b-f22649390e23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2ee0797b-9359-4499-8fb5-b0a604ad3231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 32,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b176633c-d296-413a-92fd-05f4ba6d323b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8db1291c-4978-4247-a86c-50b0b4532b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 15,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e30cb2a9-9291-4edb-b7ed-1f67ba0e2e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 10,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f8ccf9ed-5e5f-4bd6-9e2e-97dd267cf342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bbce9e74-90f4-46c1-8449-0bf1be990a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 141,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "02d4faa4-26f1-4a64-833c-c578792a3c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 151,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}