{
    "id": "18e078c9-08e7-4e2c-8a01-8f2963c1daf6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuBig",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "820e466c-000e-4cf4-8f67-8db5fbe1ff08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "22da3b39-f633-4543-b30e-4c6c9fb4fc61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 216,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "35c0a9cd-2284-48ab-bd54-35e353a94475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 208,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6c8797cd-94b9-4c49-a7c8-ad67211294fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 192,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "04e75025-2465-4968-bd75-1ced3f6ea472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 180,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "08081632-f4bf-417f-923a-14ed01ff8028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 166,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "677efd70-40b1-42ee-9bd9-5479dc51c371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d2ea4ffc-9931-45d0-afd0-f8d2a2934e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 6,
                "w": 2,
                "x": 150,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "93bc28cb-7fcf-4deb-936d-d2525f71409b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 142,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "53f2b4ab-a56f-4c2c-8393-cd6b18d07c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 134,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "ca121401-b453-4c40-95ea-f62e54885b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "36ac5c1a-0222-46cc-94db-1eaafcb583f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 124,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f96a042a-ff2f-4102-a4a3-8a7df4d253b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 107,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1e16389e-0e26-4ad0-a366-8a68e4a03fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 99,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d39ed698-e09a-4a35-a544-8a7921491bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 94,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "59440884-0c71-46e1-a440-8018b845976f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 84,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0c2e3096-af5f-4ec8-812d-73196c843329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b6086541-6e61-483d-9acd-5946ddbd2034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "120353a4-e8bc-4095-b538-28d0146a9b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "90d01d3f-5fe4-4a59-9d74-bf3a344d2f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c945a8b8-4263-422a-8f2c-6bec653cd53f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "908458aa-d081-4712-bb15-ac7ca91d2b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 112,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "afc6aac2-e2b7-46b4-b93a-b17d2d8e4884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 231,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "05428b9a-7beb-4a3b-a674-1e7823099eba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 242,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8482f414-455a-4e5a-a022-7b330b387623",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "54ca7555-02f5-497b-9568-7728a3e04c48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d4f8c516-927d-4928-ad39-bd95d75c64e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 226,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6f4382b2-c737-4929-b30d-35e17632a2b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 220,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "bc6677a6-1b76-47ad-999c-c5a9bd414a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 213,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a3a39c6b-be1b-418c-be00-c373babbc8ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 204,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4e8742a3-9a98-45f4-ac8b-e6ebc456669b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 196,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9e7bef1b-308d-4819-9108-bc2ae112c371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d541df49-fc38-4fe5-9604-b4633d5adb35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 170,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fa80b4d0-6abd-499f-84dd-67f7d73f4fcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "219ab9bc-9953-4f52-ad7c-cf1f9f7ec90e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 147,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "43d755b0-ca69-4cba-a4be-10f910b6993a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 135,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2e5da221-29e7-4d3c-a674-dc99205d9098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 123,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b1c64fd4-78bd-48b1-8eae-1351f6864670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ce421bd6-0921-46e3-ba8f-092a46143c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9c2edfb6-b973-442d-b06a-0064f88bfdef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 88,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6023d2e5-c93a-447c-a932-78ef1c27d86b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2f8a694d-88cf-4b0f-97f8-c7d046e8de46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7a24c2c9-3a8f-4d30-a867-41584146ee80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "12eb951d-2848-46d2-b9d5-72f6c5c385e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 40,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "fa2cd699-8db5-4c6a-af2f-282a2a070fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 29,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a872a959-2ea2-4052-aa8f-779eae4b4835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 13,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5bafd48d-b2b2-47bf-96c3-20fa29935a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 17,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a4740a04-00dc-4508-828a-60da6114ecf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d3cc15fb-43f8-4b7e-bf61-c97e025fe2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 237,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "20b3a799-2d1c-44e7-9cf0-204d17777094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c123fe74-84c6-4c44-8ce2-46c23cd55df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d535c511-ea3e-4e31-9d00-25b70fbf7da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0a6a429e-7a81-48f6-92bf-a4e9eba53374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2af3ab7c-a5eb-4ba4-81d0-3e0ffe7029da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "9a25f495-b09d-4353-b899-fe02e8026453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cf764643-f42a-4433-ab46-c3052f898625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d7352744-7cd4-4434-b722-2aef573ad49a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8ba907ac-4c12-4a3d-921f-2b53841e479f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d348edd6-f14c-4de7-b4ea-65363ed3423d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "be8d4b7d-f796-46c7-afae-9276a4022bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "49f50323-7c78-4d54-95bc-6717b4442b0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ad41e452-1fa1-41c5-bb88-c559b3ff362a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "de8610d6-3a44-48c4-9539-c713d0138549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "f8614d4d-d3f7-4b87-9b0a-22940775d7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e9b114d5-c431-4e18-b1c7-60b99db2f46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 4,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b754a93b-69c2-4b3b-9589-3a0cdb2c95ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ae80b68a-8e56-4eb2-9a42-a7d7ae57c04e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6f58907c-52f7-41aa-a750-166567a5a613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f7fbd471-4086-4789-9359-4802e4f2d471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "39b6e5af-af06-4c1f-9cfb-e487f3e08858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6b33db2f-f5ae-43e5-a1c1-37d6682d9dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "911f9f31-6e77-4a64-8402-e1212c25b24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 18,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "439f2db9-3def-4f2d-b6a4-a3582e7de2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c401660b-4f33-42de-a57c-dbe8989e774f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 28,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "36b013f4-e96f-46d1-8e8e-1630ce4d26bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 218,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d0115014-b1be-4bde-bc7a-5913484ab23e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 208,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d37c06fe-99ea-4871-8da9-fcbcb99f4824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fcab11a1-4c9d-44ad-af54-4c94186f56d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 189,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "73054bda-018c-4923-9c4c-31be92fe6689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 179,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f9b220ba-7571-48b2-a1a5-0618758e9829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 169,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1bb5d69b-db9b-453d-baac-f6f0a5baff6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 159,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "723f7d67-4121-4c9b-a203-700488a4b7bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "033aa24f-468a-4650-bbf7-9c3f692a9c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 140,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "48608303-7345-453d-907f-5ae9fc53909d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "fafcb13d-a394-4d64-b1ae-7ffdc7e78151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "abe5e561-2975-4bdf-9f52-90619fc3f6a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bf8a9807-e531-47cf-b20a-00cc7ec87110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a0d4f5ba-2781-427e-93d2-04f58b779e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "62b03048-324b-4c71-b2a7-e51792d7905d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 76,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7bb31bd2-d46e-4601-9ff9-89665571fb06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d1e68c13-c44d-4e86-be48-5d3bedd163d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7dde206e-c95b-48b8-ae56-933c2eaee1f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 46,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f676babf-577e-4b81-933e-1c6dc0fa98b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e5e5d0ad-4f8f-4797-a556-46083f709fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 33,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a750f824-703f-434b-82c2-6261125438a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 243,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4b9c87f8-7342-42bb-af5c-701cd2e1d125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}