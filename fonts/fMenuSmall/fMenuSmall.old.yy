{
    "id": "4f8607c6-4b5b-4d81-986d-23e43da44244",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuSmall",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6745e2d6-65e3-406b-a190-1576051b5ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "15f26a34-412a-4c39-a972-eae2c7825915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 109,
                "y": 44
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5b9499e5-13be-47e2-b8fd-795d0a2b06ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 102,
                "y": 44
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "bfbbaa4a-4274-4383-afef-61da55a20dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 89,
                "y": 44
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2ef9044e-e1e9-49f6-9996-e8b64c9ff360",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 79,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3d0aeb26-c74e-405b-9c30-88fc6dcec26a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "02508892-4695-4c9b-813e-7dc5a6172531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 55,
                "y": 44
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ba6f75d1-ed40-4eaa-94c9-a05c9158402b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 2,
                "x": 51,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7d666128-06c0-4e25-8c82-14bbacc047e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7bf40b59-4f3e-4d97-a238-a8dc60504c7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9b6c189c-88b7-4cf2-bf6d-62856140cbfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "76c32a0d-2b9e-4cfb-880f-67571b4559c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b5c41f8b-93ef-4f37-a2fa-9398ce9cb9be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 14,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6a707a99-2238-4510-b552-9826b7631f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 7,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "da1ece66-7486-400e-869f-8e26e8784b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0ac7a5a7-b529-4e89-98f1-925c4078b121",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 244,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ff915bf5-0b3c-48b8-9365-feafd43b8b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 234,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "184be55e-31d2-422d-aebe-d3b2d8e14f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 227,
                "y": 23
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4dae88c4-6c1e-4116-9d1b-b25f496b5295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 219,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e4b2cb13-6f15-4ba9-8308-f1be4c639b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 210,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e5d5e210-4ab9-4475-8257-7c0c0815f203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 200,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6bad4952-0846-4c9a-95d0-f6bce813031b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 19,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6217c453-c937-462c-a40e-00027ffd6f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 123,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0ff449f5-1261-40db-9ceb-27826ec63611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 133,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5688797a-91fd-4f15-a299-6261ca9ce009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 143,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d5f9ac22-733b-4ea8-8317-59986b345b33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 95,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7f84af93-77ba-459f-a48a-47d3247bc4e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 91,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ba02a67c-287f-462e-8a8d-06c687155bac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 86,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a8a97b17-ce54-4320-b06e-ec768d64e595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f950e420-e213-4d4f-9af6-e3315aaf63b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 72,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3436be39-4ea4-415a-a94b-1a70cd97fb05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5d5c99b5-45af-4b8b-a958-0576af2879e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e1baf042-ca0c-4ccd-8bf6-104db93f7d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 42,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "aaef243b-dc24-4c6e-a6a7-005e1d015700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 31,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "272c0efc-69e2-45c7-8305-3338f8d28b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 22,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "deaa8d6c-7869-42b9-919a-37380615139e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a80720d2-c79e-4490-b280-bc52eded6eec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8492099d-564a-44f6-b263-076a9182fbef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 236,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5f318509-9cb0-4eef-b73a-8427b0a08283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 227,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "de660f6f-db73-4559-b611-16d667edf2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 216,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b3467840-d7bc-40bf-9a46-1f90185de34a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 204,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ea327629-faff-46af-ab5e-30aab1298a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 195,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5ff8c599-41de-41b1-9779-cd9f9654c0e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 184,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "924cc732-1bfa-4e59-8f93-b530fab5f86d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 175,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d9948872-8a56-49df-a2b4-67377078b84e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 166,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "571d1c21-e051-4b84-925f-21ae9a9064e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 153,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "587452db-ce35-4ab0-8324-310e0c916c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 188,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a5f6c37d-ef21-41ec-8356-44c3b27f0165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 176,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "5e832530-f763-459d-abcf-e7772535633c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 167,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "814631c0-01f4-4006-95a3-7d8e15fcea8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7be99510-6dcf-4727-8e82-ad6f606371fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "7f040fbd-25ec-4509-a4c1-1430a326b059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "20a119d6-facf-409f-9f0d-6664f47340c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d5a43ee3-51a6-4740-a2f6-d2ead4b5b0fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "13b0264e-cd25-4b8c-889e-548649a20b58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "de1c13bb-823a-4eba-ba91-c0f4a60adbde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ddf8ca43-0dda-4e27-a5aa-0489d2fd8a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a3cf4d57-74e7-486c-8ab9-f1c22f742d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "92085cda-1326-4e0f-9f19-d130f8a16f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ab269958-ae83-4d1c-b39e-63b955f05bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4bc9f818-4a76-4b1d-a91d-af55e2fdade3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "cd11b42b-673e-4998-a283-891f3c90a208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6c569f4f-344b-426b-9661-b1e03abeca9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "46c6e805-bba2-4092-95db-75839d685464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "afe90d01-e9cb-44ca-bdc8-ff9a0423bd47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "85a3e381-b305-4c50-93ec-22e8f9dd5157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "46a1681c-6547-4933-9773-719414bf35d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ff46fcc6-1c6d-4bce-8d7b-2f3df7df234b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0ee27b65-b189-4c03-887c-909c0adb6f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1767a431-a032-4410-a786-19bc50dae293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f5cbcdd5-90f6-4d8d-8394-d2a06f7e5bf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "85e44efb-75fc-4462-b2de-6909c12d84f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d3abe8ec-aa4e-403d-897f-23b2276bf460",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 67,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a0596e65-5ad5-4093-8d37-b4e5b9494277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "66b3f2ac-5ffd-488f-b28c-c4c1de498dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 151,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "316ecf06-fe67-4493-9bc6-727493d96a16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 143,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "74b97338-b4d5-4c80-8487-77ac69d6bb41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 139,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ff660523-4684-472e-9069-3b2639525571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 127,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e414a78a-7761-4158-90eb-57c3d40986fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 118,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1eca8de2-9a9d-47c9-a4b6-133bc2e4f0db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 109,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6a5fbfae-0e32-4b0b-923a-10d3c4520869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 100,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8975ce84-7dce-4d90-9d38-efb88d9bc54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 92,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e3090906-1785-4505-9c9d-cf6b0fc615f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 84,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "228606c7-da6f-4445-b6f9-815af9811fe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 159,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c228de2b-f94c-4b89-96b3-5b0039bd3a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 76,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ea5c3edb-96b1-4870-8dfe-9c72c51681a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 58,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c4fb7268-d333-4ee3-9c43-65dc68a1c635",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 49,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e3a5b9bd-f439-4573-aba5-e0a1071c0f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 38,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "610f75ea-188d-4c9e-a2e6-02ef9a572773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 28,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e10ac00e-55cd-4dfa-9481-dcfae634e07f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 18,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "49602288-7cc7-4f4e-b397-0407017bc5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3c74ca98-39dd-4257-9e05-53a430553e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3fb2a7c0-3510-44d5-b531-7536b9ec2b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d5a9e63a-9bac-4c8b-b6aa-a87582b03c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b9f23032-f768-46ea-a1cb-7ad1eb40f373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 105,
                "y": 65
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "9ef2f1b0-ef33-4795-8b92-41e64bd3017b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 115,
                "y": 65
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}