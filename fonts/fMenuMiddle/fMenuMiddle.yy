{
    "id": "821ff1b2-f914-4af9-8cde-8ae0074c7460",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuMiddle",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6a4b9cc9-62a3-4dbe-b974-27f9995e55ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "789d514e-2184-4aaa-b15b-901a7b61bf20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 239,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "55f3486c-3b69-4464-9cc5-abef1618924f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b060e85f-e625-4e3e-ba57-6021da8d269f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 213,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e63f3860-3766-4a32-baed-1fcc5e909054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 200,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1b53f798-e4cf-473b-b973-10e3437122cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 185,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2e978071-caed-4cb0-bdf8-03e4235f2622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 172,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ac85a10a-00d3-49cf-9183-b584065479ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 167,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "af38d7b5-6efc-4baf-ad3b-300aa473faa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 159,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9567fe8f-ca00-40e8-958c-e9b4b36a689d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 151,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6550d98b-a119-4f23-9d49-0b2b7dc358d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 244,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1bf03fd9-edc0-4f96-9443-a0b7a7a69116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 141,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1f9e7aa2-9047-4ec7-8f52-873495f11fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fbd7cb5b-f5da-401c-863f-0736ffeb986c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 113,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3dda8fd6-df3f-4b77-b1da-df4cbb6fa001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 108,
                "y": 50
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c4ffa46e-b112-4f73-ba9e-b9ab60d70858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 50
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8633124c-5147-409f-8b7b-539ce7745358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 86,
                "y": 50
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "9ba53fd7-9725-48ab-98ef-e72daafa7a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 77,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ca3252aa-f77a-4b63-b8bf-264847de36e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 66,
                "y": 50
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dcd5881d-7c0f-49c2-a99a-6e9ed86729c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 55,
                "y": 50
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ef8afd1a-df33-47c6-aaa7-480ec8f3b1ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 43,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e47a8101-c9d5-4852-b62a-2d620fa692b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 50
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "72275aaa-15bb-484d-be03-70ec579f5e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6f786095-51e8-4d6d-8a8c-3fea2beba9ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0bda2e50-4a4b-4bbb-8ba0-7514f1be73bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "89c20dcc-e82f-4965-a954-37a2217fc0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 22,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "37c94b71-ba10-46b6-9d85-ae031060c069",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 17,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e07cc310-e002-4884-bbe7-735c12a8da24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 11,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6319d4f9-0d1f-4f16-9073-782ac49955af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5682635f-b8ef-42d2-b989-95ee9b3991f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 238,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e84183d9-6564-4531-9cfb-28d434dc86c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 229,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "14b38c96-60b7-480f-9ea6-aa81445da4b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7b0d98b0-a369-4648-bebd-d09f5ed85d66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 201,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "133f52c0-f5df-4d4b-a1a0-5c3abb96e4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 188,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "67128b20-c3ae-404e-9bd2-257b2e0f6863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "329052e7-dcfe-40b2-aa4b-23fe68b04012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 165,
                "y": 74
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5e38bf36-0ff3-4089-b5bc-969957699b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 152,
                "y": 74
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f913d96c-0740-42e7-be53-0b9bb82b1d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 140,
                "y": 74
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4c479b28-2d8b-4d21-bcfa-94aa8755f9f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 128,
                "y": 74
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "70904295-a4e9-45e3-a493-21d9bb4c1f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 115,
                "y": 74
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "31948894-5189-43b6-a5e2-dc3fbedccba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 101,
                "y": 74
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "245b1f30-0988-4d38-a8b6-8c5da1204c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 74
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0af3a6e6-5503-4426-a0ed-8ad9fc3b21d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "689b3b38-8599-4bc2-a58c-018ae0450852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 65,
                "y": 74
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "892b644f-d511-4afa-87b2-4b4527e81c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7954cc8c-1896-4b1e-8ad8-ca7be0da2fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 38,
                "y": 74
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2dc4412c-0fb4-428c-8db8-001b748bd8c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 28,
                "y": 50
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bac71bd5-a61f-4634-831c-09876a7fbc63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 13,
                "y": 50
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f70bc3d9-5db3-4be7-9b2b-d4d0e2835b60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "df9fc262-22dc-4891-85f5-97780093ffff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 9,
                "y": 26
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "6a7da1f5-e54c-4109-bb81-7d23b3783111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "529f6fbd-e129-42e2-94cc-1c13b1f640eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3d3215ce-6ca7-4789-bc6f-addd45745adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fb4075aa-11b1-43a0-abfa-4f6c41fd4ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "207a8f2e-3f76-452b-bcc3-2b730350c930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "61b6974b-9c76-4d1b-b5c7-86338a7aabc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fc247529-e800-4a98-9376-8646220782b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6aa6e775-321c-45e9-b0ee-f1b5833ca5bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cc526c41-81d0-4fe4-a21b-1b8ca3712cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2451feaf-716c-469c-9d69-c15d56671bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "17ac1fc3-e0f8-4698-ab34-72437d3fba99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6c45b6f8-f303-4ecd-a6b7-a7c07adc63fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a439d146-8b71-44b9-8283-2b4023ef8270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1a9cdf9f-9d4f-4519-9b85-c50cf6d6b999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": -1,
                "shift": 10,
                "w": 12,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4dccfc17-6b45-4425-87e0-b3f679364d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "52806335-9a29-4179-996d-be735c47536b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c30580e4-0ef3-491d-b92c-4ee6cc659878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "8c2c466a-dc67-4924-b221-2546237bd8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fd7c29ae-1102-4c38-8536-74317ca91b6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a4725843-bc41-4356-bc23-55d288219de7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e9a3c9a8-2887-449f-b744-78f59a765e85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fbcdf23a-2936-4af2-8e98-9f494b7004b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 25,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ee38aed4-3230-4921-b395-fb3acd055f85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 131,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4c8bab6a-2d43-4a9b-b11e-b60d4c3d5be0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 36,
                "y": 26
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1076f425-756f-447e-85bb-a7d1611f611c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 235,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1c883330-662a-4d9b-b8ca-89d6f314da59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 224,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f137a3b2-6de8-4af9-a52c-bb15e03ee993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 219,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f0bb70d1-49a0-4442-9853-67c3d2f28a9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 204,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6bd12374-e1ce-4e39-8839-7be39de3b6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 193,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "33d933ce-23c1-42ed-b212-a4767014e867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 183,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9788136e-772a-4c45-9464-8f601adf3d11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 172,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "eaa92358-3440-4c22-87b7-3bc4954a1c33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 162,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f564715a-4a0a-47b9-8dc5-9458e4cde39c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 152,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e438f472-19de-4ceb-98b7-2f2bb60b6b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 244,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e4e48e71-95d3-4d81-8af2-8a95f6525cad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 142,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "52b6021f-98ba-4641-8d75-32e42a9e4065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 121,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3a41c555-e225-492b-8b1b-972ea655a504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 111,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e80e8ed3-6118-406a-b049-229b93d8fc34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "eed4833e-42c6-459e-8479-922058a2e192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1cdb7fb4-2e47-4d7a-957f-e158c08fec1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 75,
                "y": 26
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d230c932-b252-406e-bc83-7dc80f4d392b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 64,
                "y": 26
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4a7da297-571a-4412-b3dc-5c2147ed29a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 55,
                "y": 26
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "38c35218-6e8d-4ef1-bad1-8d0a0357b66d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e0d3d6bd-814d-479e-96db-0231ca3d3f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 41,
                "y": 26
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "176b8774-e9a4-496a-bc0b-e4333b6a6772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 34,
                "y": 98
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "17dd4a70-3cd6-4327-9b07-efefb656112d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 46,
                "y": 98
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}