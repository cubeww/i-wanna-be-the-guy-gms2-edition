{
    "id": "821ff1b2-f914-4af9-8cde-8ae0074c7460",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuMiddle",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "66c4052b-046c-4f8c-a10e-65a4ad7c6e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ea749aad-b87c-4e24-b202-f32f9c92f0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 132,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "25d1be57-8ba8-4765-b2ad-25f135ff1526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 122,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "86f211fe-2b2e-4fa6-bd08-1ce0ace5ead0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 102,
                "y": 92
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0714173c-ff17-43dc-b95e-eb529f20812e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 87,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "13ab8c4c-4363-4605-aa3c-4c57f1bbca1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 69,
                "y": 92
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "66620742-e090-4c18-a782-3a6f6c63c752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 54,
                "y": 92
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "034a8ea8-8fc4-476a-917f-78bd6ca98373",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 48,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3dca9c0d-574d-4475-ac70-5de4cbee5d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "06114e40-a5e3-4d5e-87ed-dfdc1cd67f1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 92
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "647e2d04-49ae-4d70-8ce3-8cf43311cc17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 138,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "26423f77-53f8-4e0b-981a-8ecfb9743f7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "06bd7883-93d7-4501-9f59-8b5d8390c4c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 239,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0ea9e73b-0d6e-4d0f-8f37-8f5f45460233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 228,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "062183ac-d4aa-4cdc-9f7c-5f610d7d3390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 222,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1659057a-fc89-4aa7-a2ed-1afd69d21d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 210,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "19166676-4780-4523-9d1c-56fe7deca2f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 196,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "31593105-ee8d-4bff-8a6b-aa09bf661587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f0ee4a41-8459-419d-a78b-80df2ef7aa4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 173,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3237d13f-68e2-4428-b232-3171548138ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "516c6c3c-1504-4483-80b5-6da61b067844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 146,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "62ba1e02-d1d4-427c-a0cf-4b966aae945a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9b2091bb-ffe3-416a-ac9d-57437ca4407a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "14e101ad-cc5e-4aef-9dd6-a9fc27879e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 164,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "589da715-ae37-4c0b-a05c-bd0e6b2efc9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 179,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "07399d85-41fe-4ea5-9d35-d3ce8d168387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 231,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8c3790ef-1a5b-41e5-8f67-299346d11d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 225,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fe3f389b-55f6-4eff-bbe0-d9e1552547c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 218,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "92975e47-5b52-46db-8139-c139db26acff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 208,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b8b15009-21a2-42a6-9022-8c062b2c6c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e3afab7a-8454-4c0a-8248-f96f45c7e541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 185,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fd55ba1d-ef4d-44c5-83f5-1af61ad7fbc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 172,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "281e6653-98d2-4aa5-bd17-3bed9bc52de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 152,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7e7303fd-acac-4917-b151-92e30c0f468c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 136,
                "y": 122
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "60e05514-b095-47fc-92a8-1d0c4a54de89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 122,
                "y": 122
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "45f0cb3e-3da2-4223-9722-f89ce30f49bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 107,
                "y": 122
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cb571ee9-eb21-4128-ba14-a860613035a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 92,
                "y": 122
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "573871c6-9a60-4833-bb06-236d031909c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 77,
                "y": 122
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "029501d2-c866-48cc-a70e-ef27bd01825b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 63,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2c331cbc-3a5a-4d19-8579-006c3a25d9d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 47,
                "y": 122
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6f27d996-e8ce-46f8-9a83-320e991ea32c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 31,
                "y": 122
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "58113812-203f-4983-8f2a-70a5a2d9a498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 18,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1cb68e91-8a76-4e9d-83d0-1ce82b419b04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f785f36e-77d5-4115-a17d-c50c2d0ffbc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 227,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0c300281-4e21-4365-904c-62708b8c013e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 213,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "bb557e4c-8ecd-4c29-895a-25923ef0b6ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 193,
                "y": 92
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5c5180fc-268b-4080-8516-7103c6b72299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 128,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f9743f76-cafb-4b16-a582-71d802b99ea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bb840ca3-3966-41f9-9c68-c35ea8b2e275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e09db8de-2cb3-40a6-b156-0887ccce229b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 59,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d4722f51-87ab-48fd-8386-3d0ddd386f87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "df1e847f-5315-47a5-a339-e734a7c7e8ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 19,
                "y": 32
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ff07339f-c0f1-492f-bccd-2be4c79db2e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f369528b-7c4f-4ede-bbcf-40a3f143b60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0f1ff5c9-7b1f-4364-8f93-deb364406695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "39e94b43-fdd5-430c-8f64-b3f9a8b08f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b2c97c68-ad9e-457a-982c-df37de8b5ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c7f1bb5d-a31c-478e-a294-8c6a0635f84e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a8ae6c2e-15ed-4d5a-99af-6585d4ec3172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f898f643-46d2-45e8-9ded-56a792bc8b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 50,
                "y": 32
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b052eaa8-a229-4b8e-a61b-2d93fce40de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "77148064-62a6-45c0-be6a-8969017fab67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "ca4bf55d-e0fa-4888-95c2-b4074a89c556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "93434102-a6af-43f5-a8ee-edb3905c279e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ff8e031b-0ad7-420a-9b7f-4758c42712f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a6fcdcd8-44a3-40ba-b6a3-44e8c0af04ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cecb7d0d-16ee-4663-abf3-8f1442ff2808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0e79a3cb-fe74-4989-af48-f645958ac62c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "dbb4f24e-9428-414c-acfd-ff8164a71ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d99b2377-9c2e-419b-b0ee-26e19dc19b34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "94f6010b-1b18-4759-8482-853f4f80f247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "450794dd-05bb-4008-b811-e42e24eaf655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 79,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "de0bd28f-d191-4aa3-8b66-0103568361b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 205,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2b21fec6-2de1-4fef-a53d-da166da288e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 92,
                "y": 32
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9f1b6cc8-8757-4e17-bdf1-462e0aef1106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "26822585-fc05-48ff-8bb0-b207aefc3a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 63,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "98e33713-5813-4487-bdda-91ef891cac91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dda1997f-05cb-4fbf-916d-cf204b8ddba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 40,
                "y": 62
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d03cc6dd-6cf7-4bd6-ba5a-7d732b94be9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 27,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "e04ae8a7-3f5b-457a-a089-5be2190f9e99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 15,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "913b1456-9d67-4e0f-a844-7b63e9ae4285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7a833aec-c544-4d98-a34c-af3dff5abc60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 241,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "2cad1486-7678-4f12-b134-76f029c3e04c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 229,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e8b9efa4-a6ea-406c-9cca-3e7bbaac1ca8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6ab8052a-9960-4f91-a974-df813b3422b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 217,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "de7602b9-527c-4910-be73-4be73abda0c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 193,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "06c8a814-a44e-4296-b351-1ac03695fbba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 181,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b20d0798-eb38-400d-ba1d-d03ade2fd98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 165,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "989d5e7c-8916-4872-9ff5-fecb34ce32f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 151,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "7de78e22-95e4-4fdb-b5b7-db5081c191ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 138,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "130d640e-79c3-4214-9155-fd1941a2e852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bbada1d2-d0f0-4bda-898b-e7c862826088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f3080bbf-85a6-45c8-8105-d7d2f303f130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "6b539ce4-cd36-436c-9dcd-bdb025838b30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 98,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b905e059-6cca-48a6-92a9-d9adf6ed0eb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2a035f84-1176-473b-a3b2-89097532ed9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 28,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 16,
                "y": 152
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 15,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}