{
    "id": "18e078c9-08e7-4e2c-8a01-8f2963c1daf6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuBig",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "010a4d89-c11b-4976-a0a3-3302a06eeb5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "579e937d-fc92-48ce-909c-35b8fe0e8cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 28,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 132,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "782d5e00-75a7-4575-a0b2-a69c9fb41578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 122,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ab5c428d-af05-4878-97c9-d91337caa7e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 28,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 102,
                "y": 92
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "587916c5-1b4b-45dc-9866-50d4011875a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 87,
                "y": 92
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0320564d-f7a6-4ff0-bd3f-dc7452128a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 28,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 69,
                "y": 92
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5bbc59d9-91d1-4fd3-9d4c-48d1b5ea33d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 54,
                "y": 92
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "65c8eac2-b8ec-4e03-a016-1630490de6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 48,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8e7fa2a3-6f00-4b98-a7aa-fdba88e55a8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 38,
                "y": 92
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5f279772-f012-4b4a-a0c4-e1aff4e2477f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 28,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 92
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "16875d36-13f6-4b7a-9bf5-dcaf5779611e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 138,
                "y": 92
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a94a5d6e-51de-4148-a3e1-931992390dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 16,
                "y": 92
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7927f3a6-d920-4563-9cd8-c9fcb8e350c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 239,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0d1b4100-c1cd-4385-b337-d3e81c07051d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 228,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0e9dba8f-52c2-4dc7-a70a-22387dfae67a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 222,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8007c85d-3a3d-45d0-99da-e78806090d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 210,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "67d5f02b-3f25-4faf-923d-1aa293eae80c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 196,
                "y": 62
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f83f8388-58de-4a12-98a2-1cb0ed7db0d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c7f8d361-0a9f-4d77-bbd4-76fbe864b184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 173,
                "y": 62
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "362e7c70-cb19-4042-9e68-edf2fc5b5aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "456c36d8-b21a-4e0d-8ff4-c18c2d3a6a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 146,
                "y": 62
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "588a1c9a-dff8-43f6-a770-dedac524c817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "adba3c38-1f58-4ae1-86fd-2390144247e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 150,
                "y": 92
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "78ff9a70-0889-4bc4-88da-fabe3a01cfd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 164,
                "y": 92
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "5e477b8e-3790-452c-90e1-ed1f20a32f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 179,
                "y": 92
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "23a71fa2-c537-4995-8b34-2880bf8649dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 231,
                "y": 122
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e5342b9b-184e-4f1c-992c-9c739ebdb9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 4,
                "x": 225,
                "y": 122
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b819e7fa-586d-4395-a4e1-148d8bc7a1c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 28,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 218,
                "y": 122
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6468907d-4d29-4f3c-8634-de2aafe63732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 208,
                "y": 122
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2b8fa559-26ff-4e6a-892f-b43e3d13b54d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 122
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fe0c1b29-1df5-4cbe-8522-7a960e02f394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 28,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 185,
                "y": 122
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "77770f23-68bf-401d-bac4-9e88f3b8a7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 172,
                "y": 122
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4bd7f845-dedf-4152-b9da-4e2b740a73e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 28,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 152,
                "y": 122
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e0306512-e776-4357-bacf-057fad2fca51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 136,
                "y": 122
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7e62ac6e-89b7-4a00-87fd-cccc8e5e4cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 28,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 122,
                "y": 122
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9829b1a8-dfc1-498f-a30a-7326bdd38bfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 107,
                "y": 122
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bfd94e13-9c07-4658-b320-f54e6be2d198",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 28,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 92,
                "y": 122
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "34bf9b54-1b1b-42c9-b330-0aa7e4c06de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 77,
                "y": 122
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "426594bd-80a0-4b53-8aed-ca6843ac379b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 63,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "250f6299-c463-450d-9f71-53906b3d1e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 47,
                "y": 122
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2e4dd8dd-c5ee-482d-a586-cd448fb46eb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 28,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 31,
                "y": 122
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f4df8a0d-22ca-4178-85e9-1cbe5d2d7d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 18,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b438f38b-9704-40f3-9435-e2ee59d0a118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a13cea66-9bbe-4bf4-a0d0-2584e75d17da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 227,
                "y": 92
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9a85f7f4-1ec2-4316-8a28-c05a7e5fc153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 213,
                "y": 92
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6bd54bcb-8930-44e0-9902-f7301eec7752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 28,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 193,
                "y": 92
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b680c3f9-1193-40be-b672-0e7330469588",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 128,
                "y": 62
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7714012d-6481-4b40-9a6a-e70a820001ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 110,
                "y": 62
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d17c4aa8-ec0f-473b-bb89-96a7d88a2ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "63d8cf70-3a73-48ab-8ed5-d6b440e423ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 28,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 59,
                "y": 32
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "088fc3d0-f85d-4654-8623-bfc82765718b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 28,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 35,
                "y": 32
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3c899f9e-6f8c-426d-bca7-1b196c4839f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 19,
                "y": 32
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a67cd8e5-0ec4-4478-b5d3-44322269ed66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5fbba587-651a-4963-bee5-1fadff35eba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 28,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "2916e80f-3b17-45c9-9e97-c1529f696416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c5e13889-4348-4a74-a2ad-ce1a2d6319b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 28,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "99f8d961-ff2e-448a-8ec1-83622cf7f425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3029f369-5656-4ac6-a18c-6686214f6fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 28,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0b6491fe-15ca-47d5-a448-86da0aa1a88c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "630a2dca-9ca0-44b8-85bf-7a6f6c37e8ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 28,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 50,
                "y": 32
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "97e9e472-f07b-4c2f-92ce-9c7e1afdc618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b45096d7-a814-4feb-b6fe-63b2b33e925b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 28,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "61c1e74c-a070-4b20-ac96-2df8b632ca4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4692eb00-ba3e-4393-9bb3-f540951cd9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 28,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1d20c36a-7607-404e-8973-965574d6093f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 28,
                "offset": 1,
                "shift": 11,
                "w": 6,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "92bf3aa3-0134-481d-a439-77479f03bf74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bb7b1de6-a6ad-4ad1-a35d-ae66002b6d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "73ae04c4-8a77-4dca-8f66-bf85b036a5b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "34e48307-3478-4582-91a4-a60e8c1091d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f772e6e3-6b7f-4bd4-87e8-6672aedcb7e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "dc87c3db-0e11-4f08-a74d-5a550d7fe0ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d47f41a9-a398-40b7-ad6f-042bd53a4ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 79,
                "y": 32
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3b979b99-cb6b-4cbe-906b-4b0b5acb20f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 28,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 205,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ea359579-e16d-48b2-90a7-2b5bc4021e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 28,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 92,
                "y": 32
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "80431911-544a-422d-835f-a19b553e84e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 28,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 76,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1c5559eb-0e89-43d8-97e7-0482a671eaf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 63,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3ea814cd-1687-43c8-8fbd-b79f3bd8e638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 28,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0133d663-b1bd-44c1-becb-46fa2f3b1431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 28,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 40,
                "y": 62
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ce8b4545-94b3-40ae-82e5-261273c0a0a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 27,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3a25bd88-54b4-4a0f-96df-ad8df5e20194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 15,
                "y": 62
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f864f5df-2f2d-4ac7-9e42-0bfab5019a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "84bf6efe-d010-41d2-a078-9c6893a53cc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 241,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6bd6a9b5-6e5a-42f5-940c-994de8e64200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 229,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "bc2297d1-5040-442f-8210-7e9e4fe3c95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "39283d64-8df7-40f1-8d36-d2cdb3b5d94b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 28,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 217,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "726ceaa1-ceaf-4a2e-8dae-8812ef050a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 193,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9d56b126-9c47-4ea0-8a17-7865885506a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 28,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 181,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d3ba02bf-b32d-4475-8816-7f6ee7d8d084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 28,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 165,
                "y": 32
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4b489114-088c-4537-aa38-a1d8c26c5551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 151,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0b19c68d-f0d8-4bef-b4f4-e1129fb7cada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 138,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "bcd91692-d474-485c-a189-3c201753018a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 28,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 125,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d75e0e8b-b99f-4d3e-ba15-bf8b24c9f21d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 28,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "99300675-2a99-4487-b264-62e80109d7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 28,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 109,
                "y": 32
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5a30457b-3132-4344-98de-d7b1965c178a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 28,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 98,
                "y": 32
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d59bcf43-f30c-4581-92bd-6965428784d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 28,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2c615e5c-76cc-41f1-9116-72b547d109a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 28,
                "offset": 4,
                "shift": 19,
                "w": 12,
                "x": 16,
                "y": 152
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 15,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}