{
    "id": "18e078c9-08e7-4e2c-8a01-8f2963c1daf6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuBig",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "15386918-39ad-4d5f-b5af-42dcdb6caab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0ba785ee-f6dd-4bf2-9165-4c96be1d1131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 102,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ecdd2c52-0db1-43b4-b958-4aacf53e8fed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 92,
                "y": 86
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e0cf178c-e44e-490f-a375-aec0e3d48106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 73,
                "y": 86
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "02a3dfbe-5ee2-4418-b1e8-01032bda331b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 58,
                "y": 86
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5e6286a9-a8d1-4242-9d0d-a2b794be2bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 41,
                "y": 86
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e850e49d-7c33-4d57-9e81-5fec174f1951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 26,
                "y": 86
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c2f050c5-e9ca-4412-869f-72411b36d92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 20,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a45e45b0-8a31-410e-84e2-b53dfee3556d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 86
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4ab59f82-4ee5-4020-b66c-946e3711cb2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0de80be5-c886-490d-bc18-a5c72b779428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 108,
                "y": 86
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "afa60c3b-d141-4553-b8bb-cd641536c7c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 238,
                "y": 58
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4c0f2ca8-a81d-4581-b134-6449669a7c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 218,
                "y": 58
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8da1bbf5-763b-4a1c-ac22-85e5349b4076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 208,
                "y": 58
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "02ed8cd8-d078-44bd-864d-1d1e3f1193b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 202,
                "y": 58
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6633fb55-9bae-4381-8c98-9e2edee373b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 58
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0f39bb2d-a60d-43b4-9aa2-403e3b8f3638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 176,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "aa967447-d967-47b6-909f-7daf272bb5ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 167,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8ebb16c9-1a75-43d3-8abe-eeb1792953fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 155,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "25369eb4-eebf-411d-a98b-1730ed7ab910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 142,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3c080142-bb0c-4153-bd61-30e6b2a134b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 128,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8a9ad5a0-4f71-4403-91a6-c18f73552cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 224,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "edfea374-cf16-45db-ae62-7accec5125ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 120,
                "y": 86
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "03e8cb04-9f93-4766-9974-f574ab7ee65e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 133,
                "y": 86
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d158fe23-9ff6-47c2-a789-08b44eff2aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 147,
                "y": 86
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "19621a28-76b7-4045-bee0-3abc84f76ed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 176,
                "y": 114
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "754b18af-3cb2-4a48-8129-45e4df2f2638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 170,
                "y": 114
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "de1cd54d-3c45-4223-b82b-d8125710bb0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 163,
                "y": 114
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5fdcbcd7-ca1a-4176-95bd-6814b8bb9be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 154,
                "y": 114
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "85acbbd3-0861-4624-bbf2-0e0a958ba548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 143,
                "y": 114
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "81cf385e-c87f-49d1-819c-d6e64e388542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 133,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c44cd830-da39-48e5-9688-b39eecacbbca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 121,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "dfbfca18-348a-4415-9994-59c16ea64c4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 102,
                "y": 114
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0ba3a1f9-0b41-4663-a826-e02a36f7127e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 87,
                "y": 114
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8ae45f72-c6b9-46bf-9582-fcdba1a850d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 74,
                "y": 114
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c1d75efb-8884-49ac-a49f-02ebb03033f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 60,
                "y": 114
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bdd91156-e612-4af9-9d94-0f26848d01e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 45,
                "y": 114
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c8fbb1b0-8744-4899-aaea-ef3ec088e323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 31,
                "y": 114
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bac66ea7-c3ae-4643-a6ba-8352b0a2e6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 114
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "320dd420-d017-4497-930d-b6ec74927115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "be4edab3-368a-4e45-a37a-21dfa9b90e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 233,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "35cadd92-ba73-4378-b14a-be1299665e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 220,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "11c15b0c-c831-4489-80a3-974699cda173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 205,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e312592c-7d83-4973-9c0e-66b27b0f06da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 192,
                "y": 86
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "0fdc180b-9c13-485b-a9cc-60d42f919088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 179,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6d1b9e59-85fc-43b5-9af6-f4ff6321f7f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 160,
                "y": 86
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f40c7e30-97cb-4684-ba00-e084637ebfcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 111,
                "y": 58
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "eb8dfa89-09a6-4a7d-af59-021df6b73da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 94,
                "y": 58
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6d355760-20be-441b-bb70-710c47818b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d8dcc0fc-5977-44ea-8bf4-b6f8254f3ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 55,
                "y": 30
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "52e162a2-a84a-488c-98a0-632521dc7a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 33,
                "y": 30
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d8dd9712-623f-4302-8ebf-0a6452ce422e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 18,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9cdef4d1-7a72-4ab3-975c-cef096a39d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "52b5d7ff-555f-4102-84f0-bea526b9480f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0d307c5a-b1ce-48a3-9924-edb048f49e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "aa9ccfc8-996d-45cc-a734-a8562301e835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a18f1f8f-d9cc-4452-b0da-1f770c3c4624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "082abcda-4032-498d-8738-006c6c80b03d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "71e3de21-2d77-44e0-b9fa-cfa01b146e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b3a4b42d-2355-45d3-9a79-2fb2026f8953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 47,
                "y": 30
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0db36264-e335-4c6c-abda-df830e0e786c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "4dfbc7f4-9ae2-4fca-aead-a9cf54fe3948",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1819b0c3-5f06-4b19-a709-b39d0c5d5d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0bd0c1f3-366b-4b3d-a1ef-1ef05c90d893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "94098437-9e75-4828-9fba-20a8859fa11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 1,
                "shift": 11,
                "w": 5,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7bad4117-0697-4cad-b7a4-1cf7cef4f307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d2246048-e17d-43d9-887e-01393a07dc42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2ed3d75e-3488-476c-9eb4-693a5f414bfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "30018975-1024-45c1-8616-8789e30f2db4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "775844bc-1068-4626-9db0-e736ebfc9b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b6df79f5-855d-4332-9b38-f178a97c3f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b9211528-c499-46a3-8f10-040064c8e578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 74,
                "y": 30
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6053ab8f-a4a1-4ca3-85db-286e1e973b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 196,
                "y": 30
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "27340070-4de6-48b3-b829-d6117c12d3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 86,
                "y": 30
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8e9fd6ae-abc5-4564-bbd0-291e2f729d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 58
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "39a06d22-bc0e-41d7-923f-3e60779392e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 48,
                "y": 58
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e28f5ff5-36de-45b2-9cd1-82572eb9c856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 43,
                "y": 58
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e9e28c9f-09fa-4709-8799-d0e5e43c14fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "43669633-d541-4b7f-bff6-785a1f1ec29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 58
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7ea9a805-6fd0-4246-bd20-07385d460d53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "05503ade-d2ce-482a-bf3d-8cc53cea4766",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 243,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "540d586d-7cc3-4e81-9ed5-478c125239f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5afe426d-0d59-41c1-9be1-d4ae7dc776c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 220,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0cd5d3a0-d4c8-4028-9793-6612be9c6897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 58
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bde1174f-f72a-464e-a207-9b1e88b3dba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 209,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "434a1073-fe52-4260-873c-dd175e2eea92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 184,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "91f57917-131e-4589-bb93-f9518f7e0272",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 172,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "60658d94-0baa-42a5-bdce-b46cbfb278da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 157,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9c66c3dd-d08a-4d16-b92b-dc65549490c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 143,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "78102616-9fbe-42ea-92ab-edb58f288558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 130,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ad7e6f3f-3cf2-43ee-b9ee-25709b80ee85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 118,
                "y": 30
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b96e730b-723d-4bce-8d60-0345c48f0db3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 108,
                "y": 30
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c3908929-73a9-49f4-a2ab-98708c65f378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 102,
                "y": 30
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ce2140bf-8976-495d-a523-d470bac88369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 92,
                "y": 30
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "65e4987b-aab5-4d77-8c97-da0fc4a8f97e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 189,
                "y": 114
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "09e330a1-2073-4416-82c2-20b48e56e5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 26,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 202,
                "y": 114
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}