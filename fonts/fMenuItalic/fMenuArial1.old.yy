{
    "id": "3d6375c8-0f87-4858-98f0-8803a94be2e9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuArial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6e739070-3036-4ee7-ba37-3b93e7dc7911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ef7e6360-3192-4596-82cd-89976608770c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 123,
                "y": 36
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7d1cc7d5-373f-4a14-a73d-b8ad2e64fe8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d2b42dc6-6eb3-4976-bd30-4c8fa057ecee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a3860f0b-26f2-4be0-886f-fe073a4b03cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "1713795a-c73f-4fef-8625-7380a0a4b027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 83,
                "y": 36
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1faa63fb-3c3d-4bdf-99b9-ff1e14f7d018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 71,
                "y": 36
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6e2f8ec1-7b94-4bdb-a322-622121c4b736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 66,
                "y": 36
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b3b9d01c-78fe-480f-9e0d-ee79d58992d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e630551d-5b66-408d-a036-bd6df57f0fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 36
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "68916196-c32b-4cc6-91f8-f3b7baea57da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 128,
                "y": 36
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "dd50f3c2-527e-4b3f-83f6-2feaad10ace7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 36
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d4624f82-5722-4475-9245-654ea8546003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 30,
                "y": 36
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6bdecc00-7800-476b-acb3-1e32e0635faf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 36
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1e2cf4b3-3f81-4051-9bf6-89c3f15e1eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e5a0f5fc-8b5f-41c5-b490-1b5a19ee878f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2be7b864-7cf1-453e-af31-380688f074b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "78be37a7-6a05-40fc-947a-48f3000569a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 240,
                "y": 19
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3a3f85b4-b7fb-45ea-929d-ed146af95074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 231,
                "y": 19
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0aab58a9-f02a-4948-9ec3-a20d97de5d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 222,
                "y": 19
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "70624aa8-8323-47f3-84db-e32363ee7f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 213,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b1d11281-ac4d-43bc-b50d-bc7e5c304e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fdff3c96-e35f-41a9-a3db-5b356832e793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "95723939-a5ad-4f99-b199-e7d232062ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 144,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "18194f43-a227-47cf-82fd-17394c7743a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 153,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0bc892c2-f28e-4955-a2d6-97ff12881253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6c0e6032-0632-4bb4-9ea0-ed362d91350c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c9dc9d95-42c1-40cd-81aa-61d2be6f69c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9f05104c-c811-42f2-8fe4-5c8632068829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ef90ebd6-fd57-4d54-9ad4-cb545354586d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 53
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d76e8772-59ba-462c-81c5-a2b4d64edb53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 53
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ed43c89e-417f-4818-8f9f-08685b86c07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c2b12018-e943-46a4-96eb-e59407065757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "78a9c5e8-23c9-4510-99f4-542326f4871f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 35,
                "y": 53
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3e76897b-aa0c-46dd-bc53-b5324df43b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 53
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0ad52239-1a21-40c3-b07e-08bdb14af435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5bc31943-9eac-4ffe-8576-6419b215d161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "262899c5-bc7c-4de4-a77d-03e271d07ee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 242,
                "y": 36
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "88a198e1-41f0-4701-8f97-14f7a3eddd63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 36
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2f1bb730-4e43-426c-8950-8d562917cac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e5d96142-bc83-4ca8-a2e1-478cd134dac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 209,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e29d1b27-4818-47a7-81ed-70e0847b0c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 36
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4f2b53f5-7e7c-422e-87f6-1b5aae1e85ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 195,
                "y": 36
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "d4066f43-befc-48cb-af78-1b1439c4297f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 183,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4a749105-b15a-481b-95f8-653a6d095de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 174,
                "y": 36
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "eaaef760-ecbf-4fab-b478-e29e7b57850a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 162,
                "y": 36
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0a7549bf-cc24-47ad-8138-f69277cfc653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "82faead1-2677-40cf-92f1-d5f99278d279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 19
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "23b0ab94-c825-41d0-ae9b-c925019cfab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "bcf352a5-f389-4ed4-9282-c7dc978a78bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ce8bf91c-8742-4ba7-ac94-7116f3813327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "72b43853-0465-4956-b5e8-bb9a1c5c6fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "81ba09af-f33b-4eb1-81f5-5d64852810d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "582e9ccd-9de0-44d5-b749-abc24d1f53b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7c7cd0ad-cb90-4de3-b889-e10cb2ceac00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "68ebd0b8-1e14-4de4-b291-6a4090d78675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "772a8983-bd21-43e2-8a59-958f6b394184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "98f07b15-d05f-4e35-aab2-c6edbe9421c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "666610df-a791-4c4a-860a-aa83a64eebe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c5161767-cdfb-4b7c-85a1-88fe3310b71d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "780aee00-890a-4df0-8409-262b579aa42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "25850ccb-535c-42d7-80b4-61392b107cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "efd37a56-6286-4927-af9d-7f5402843d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fcf443d4-478c-4a9c-a072-7dd60bf49ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "41840272-ab6e-4d72-8cb0-5cc0b759da00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "16b34acc-3a12-4553-8e0c-7f06358cc7a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bbaf79f1-f283-445b-8f29-406fb81daa47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7216db0b-800d-4680-b2a0-a65fe9bb3da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3381f57c-b1ff-49d5-b8ea-97f9df3c38de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8029b648-72d1-4f16-9250-9381e31a05f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "09a28e61-675c-498d-8de0-6e99d1860061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "622554b1-6844-4a21-b291-b025027b3079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c2f855ed-0c66-411b-98f3-feabf7e05db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "976c8218-8f1d-49b0-805e-a5c431098663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b71c7a97-9f0b-420e-ba87-8018e81e11fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 164,
                "y": 19
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "88fda64e-b6e7-49c5-9d32-b28e0dadf4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 154,
                "y": 19
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2f2001ac-76bc-49f0-9b1e-38e1a4f8797d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 149,
                "y": 19
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f12935f2-b898-4fc1-9d69-bf78b34291ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 136,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b6f14bd1-6289-47da-9ee9-7855153c8b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 126,
                "y": 19
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "baaaa394-f191-42ca-b2d8-03444bb72338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8d4f512f-8d57-4b37-89d4-19637061bd28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "53003583-6045-40fd-a5a0-a656f7638527",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7fe5971e-e9ed-4c84-9ef9-1f41bede5c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 88,
                "y": 19
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ed37d4a4-e3a5-4c2c-98d6-bc54b520d232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 19
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cf2da0ac-a248-4a97-8aac-7e127c367fac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 81,
                "y": 19
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4674c865-a6cc-46e2-b756-a42afbd3dc2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "88304e2e-e9a7-41b8-bd07-52352b792599",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 51,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "de569727-bba0-4cef-9379-d5538bf7019c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "aea1148e-9896-423b-b5be-eba47f22dca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "dd85e34a-2ad6-4e79-b281-5538cc59f08d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 18,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a1af3255-a980-4256-bd80-fb3ee77e15c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "67653f40-cad8-4c4f-b58d-ccbece599005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "af7585fa-3642-43ca-90cf-88d6c8d39117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e9333295-41cc-4016-a134-7ae869b8db72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "86e13b22-7138-4422-a01f-b9b6b6a3c4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 119,
                "y": 53
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "249f5952-57fe-4f83-aba4-f18d120cd35e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 129,
                "y": 53
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "292583f8-2e19-42bd-a643-a8db5714ed18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "b6e288f0-0968-4c5c-8740-3b65dc29fbb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "1e59472f-2867-4f12-92ce-4173112de950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "13c39bf0-548c-4975-8a17-5cb4d860b221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "db9e42bd-a480-4c83-908b-8cc2c7f9c6b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "f58fd3a9-c504-46fb-b44c-dc82deaa4832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "41eca82b-4a0d-4881-98ba-b5cbaf1aa0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "2655e2a5-bb53-4c60-9ade-4ef972c1c7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "701da0b1-dc0b-4170-8c37-ac1a485095a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "4a333e19-de1e-4f3c-8a32-6e22780720d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "7a25fccb-0a25-4b4c-8a6b-929ea82a0b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "486fd2d3-fea5-4ccc-9d11-84a4c70a4d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "3d4564d8-5e03-4c2c-a048-4ad6f0786733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "40f0d3a1-9043-4bac-80ca-7c59f071511c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "7f057d31-f7b4-4f53-b549-9f2e5b5103db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "75a31330-4ddc-48e7-9f0a-b2ed12c9181c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "99e6b862-fc29-4750-856f-49c6fc6ca700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "2bc0f51f-86ae-447f-ab1a-39699eefaa2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "37fa0dd6-a253-4340-8989-24efbaa8340c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "77f7f217-3c99-4a7d-95b3-b4f6f8f7a107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "74b283b5-e0fe-4da0-b763-a2989e1802fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "5264d4db-3a94-4133-a7ca-27384699040e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "5889ddb1-d5e9-42f9-b125-95babd042273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "a244dd9a-d392-4c64-90b7-d76b5da38030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "324d35ee-fedf-43fa-8063-eab77dffd0ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "9d52a901-1726-4b20-88a0-820fc597f17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "ffba53bd-25bc-4ae7-b11a-f6449452874e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "aee802e6-ee27-463c-bbae-c15c5d63e95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "5f797163-6f47-4668-9826-3b9e19411b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "7496a314-0acf-45ed-8780-254e85c6a18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "02ba564a-7c12-4a5a-9d01-89361ff9859d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "714711c9-cb0a-4ae0-823f-370b724e7c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "8d7eb84f-8eb5-4be1-80e2-b89464462184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "2718af31-7c07-465e-8d78-62372c2175b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "c4e35025-9b41-44cf-98fe-45ceaa5be638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "bdb08324-7e3d-4aba-8ed1-39bd88aea19c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "c403f511-7b9d-4cad-9d0a-fd040112ccf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "91d59252-e729-4e18-a804-2253d4e027e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "9e4e2c06-6d41-4483-b8b5-cbffce28593c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "c90fd88e-73dc-42ce-aa06-afb297d2e5ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "31619cd1-7a3a-42d9-9d85-b9409da4e25d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}