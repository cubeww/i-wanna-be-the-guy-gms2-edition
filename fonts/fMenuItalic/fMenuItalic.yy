{
    "id": "f2456fd5-17b5-4a2b-aae9-edf532777086",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuItalic",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "bd86a7b6-2984-4f22-a888-e6896a399b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "75285b24-510e-4458-bd12-146e620c856c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 134,
                "y": 36
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b73c9aa2-1271-4e38-a812-768d5ecd6ac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 127,
                "y": 36
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "44d5a7a3-56fa-4166-9714-5b56bc309066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 117,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "121256cd-ccf7-4954-bbbd-27e795b8ebca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "60ee9595-fafa-4c13-9636-d9be055856e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 94,
                "y": 36
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0154c0d9-1ca3-4fb6-a01c-8d911cb2b933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 84,
                "y": 36
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b0b01aee-336b-44dc-9c9d-ba5c9b69bab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 1,
                "shift": 2,
                "w": 3,
                "x": 79,
                "y": 36
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2eb2ee54-b1fd-4c30-afe5-da8268d2de39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 72,
                "y": 36
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8b4770bf-92a3-4978-bf6a-f9d29c58b5bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 65,
                "y": 36
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "febfb446-7a29-46f8-b459-de3f7c2b764f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 140,
                "y": 36
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e1cfdb5e-24e3-4e56-ab27-d60bde6217fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 36
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "09306868-c862-4d8e-a972-3bff275fe361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 41,
                "y": 36
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bb2c944a-a7f9-4ebe-9ab5-aa74663818fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 34,
                "y": 36
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ec203554-822b-4ac8-810a-51ae21143a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 29,
                "y": 36
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fd2b4674-66ca-4d72-938a-3591490b537f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 7,
                "x": 20,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "40d9e525-7455-4e0f-a5cd-bc66c81bd59f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 10,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d6fb06fa-1eb9-44ec-abb1-0008da3cf906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "7ce1806a-26d5-4481-a44f-1df6a1d2c553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 239,
                "y": 19
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "9a0f4201-b3e3-45e5-9398-b772196ef90e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 229,
                "y": 19
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "626f342b-23b7-4128-8116-3a50297e73cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 219,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c65af97f-59cd-4890-95ba-058daa6f2745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 46,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e23c29b5-5189-40f5-9dbe-eb4e2b2fe5bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 147,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a3c877d7-c05b-454f-ab95-3a2caf8c1074",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 156,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6a79c8e1-ea72-4a6a-838c-fd47748aa9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 165,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d7444064-86df-4f27-b81c-1ca4f785f621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 134,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cf4081e1-bb2e-4c77-ae44-1fda07dd1118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 128,
                "y": 53
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "98c536b7-f5f3-4b3c-a37b-c04d61a5e54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 122,
                "y": 53
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "13f2d478-d60d-42f3-95bf-199fe1d2e55d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 113,
                "y": 53
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ff3359cb-c0d0-4b60-9827-665f38e7b1de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 104,
                "y": 53
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e302fe13-c01f-4903-a4bb-2df278140660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 95,
                "y": 53
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6496f980-d648-4bcd-bedd-be361417371f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 86,
                "y": 53
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f7cfc070-1ee4-4a13-bd53-1572cd5ebc3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 71,
                "y": 53
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7a6a65de-3f29-4623-8139-3e84e36c8091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 59,
                "y": 53
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "608e91cc-b7fc-4f78-a38c-f4b034d638b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 48,
                "y": 53
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "612c60c1-2267-4dde-9cd1-b75605b2ceca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 37,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "627f5d23-82ba-4270-8df9-826cd0854205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 25,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "106f4e3a-ff8d-47de-847b-c51ea8be011a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 13,
                "y": 53
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3885ed1b-bccf-41eb-a5f8-26a32fb2cc6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d3af66c5-432a-410a-828d-b1efb7952c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 36
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "43671b12-bbdb-468a-afcc-2fde8ce891e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 225,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7391802d-4816-46c2-a5fc-b3c404fff09e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 219,
                "y": 36
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1a644a6a-bd48-497e-a921-3bb4754b65c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 210,
                "y": 36
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "56f2d3ee-f411-4ebc-8224-fb6a304504dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 198,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3296b7b8-c00e-4864-95a5-3eea5f793744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 189,
                "y": 36
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6c4e0462-15e5-43e0-a079-e36c02570178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 175,
                "y": 36
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b4496ee4-1636-4c67-ac8c-c4059df7ee49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 207,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "cb22b629-1326-456c-b809-f7d94221ea3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 195,
                "y": 19
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "95c80468-6129-402c-9cd9-d365c3ae5b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 183,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3a46c022-445d-4f33-af0c-5638c1b3538a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b5a5b4e4-73f5-4a1e-9bdf-fdd5d2585ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "401f619e-08a8-46f0-86d3-2ed11e63cfc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "7c939cfe-2914-4418-920e-a63d79540ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 9,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "03957d65-38b7-4cd2-a3e2-4405df548b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e906e79a-c255-4d28-8fde-2997c8e9c7ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "97967b66-02e1-4c3e-b1ce-350182df5370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 1,
                "shift": 12,
                "w": 13,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0b7682b0-d5dd-4559-9f08-26513f36721a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ee1792d0-ac29-4cb5-8ece-64cf38f3a8d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f375fd84-192b-475b-845e-ad6acb17a893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "49312ef7-94a2-42a2-a89a-fcc8ae47a8f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 6,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7eaa65b5-602a-4c69-8e91-60d24ea73167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7a86c15a-1e10-400e-ba5e-304f25a19e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "61d5337f-4dec-449e-808e-b61aa0f459e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "22bcef5a-9978-4a73-903a-2357b480fa97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1df55a20-747a-43e6-9301-0b2c76b6e16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a75a35c9-205a-4dc6-9a1d-294586507688",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e768d88e-a738-4f38-add8-e8d46d94ee4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4721a363-b75f-4ecc-8e3a-8effee4fba3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "3173be33-729a-49c2-a016-fd185aa43615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0d945027-dcdb-4045-ade2-30db8d248804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c7072a20-e3ca-440f-bd37-75febc4057f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d024a4f9-892a-432c-871c-19003e310d0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6be1de09-2f6a-4de7-8da0-5114be81b22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "43c2af29-994c-4d9a-8839-5b6a20eb32f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "295a50f7-a997-4128-9fbf-c061f7b3bce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -2,
                "shift": 3,
                "w": 6,
                "x": 166,
                "y": 19
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b0230b4a-b119-49d1-a7f8-ddc58267b800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 156,
                "y": 19
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "3736aece-4ccc-4e5b-a367-5fa5e2930be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 150,
                "y": 19
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ba47abf5-e3d8-41c1-9915-b5ada82ffe84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 137,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "36ba63c0-80c4-4f07-a2a9-20d0e1103b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 128,
                "y": 19
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "deca4628-c712-4f64-ac3b-817eca6bed7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 118,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "42157b78-3458-460c-be7f-8d076dff0bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 108,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "31dbd858-8001-4bab-84c5-5ab23a737b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 98,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6f4cc3ce-2cd8-4223-9f74-05a9426935e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 6,
                "x": 90,
                "y": 19
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a0e5d5cb-116d-47df-ae86-a87b2e0183aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 174,
                "y": 19
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "22eda390-025b-4c25-a2d0-cbe8b2768473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 83,
                "y": 19
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6ec325e3-197f-4467-9d2f-d4f7b3e220a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 64,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9f731664-ebb7-40b0-b2a4-e5670ca7bc58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bf33412e-c0d6-4605-825a-0bed6e315df9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 10,
                "x": 43,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1ace29f4-273b-439a-9284-aaf9081235bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 8,
                "x": 33,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a4fc33fc-142e-4876-b4d3-db34800c4cc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 23,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8f7819f8-fff9-4abf-86a9-ccfb4d3c8b15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 14,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "aedc4289-5718-4e4f-91a3-1e8e7e76ac38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 6,
                "x": 6,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3fe8b501-7074-4d35-8d03-d1e59cca3b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "19b682fc-802b-40b1-bbc6-4ed31e148f09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": -2,
                "shift": 4,
                "w": 7,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "78848249-01f3-4c58-873d-8f78f5adc388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 144,
                "y": 53
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "e24dd437-84a8-4b0f-b044-0ef5837981cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 153,
                "y": 53
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": true,
    "kerningPairs": [
        {
            "id": "ce17466e-9244-4231-ac58-0561802f733d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "15f18751-b912-4d79-bf8a-e290105a5f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "a7e2a516-e04f-475b-a5b4-3c154e0d5b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "338b070b-49f9-48c4-8e16-92ff6473c47e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "6c16f6c8-570e-4b8d-9159-5454aa0ae50e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "e1b567f8-acd4-4075-84f7-343f575fb4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "f535be9e-2cd3-47bd-a170-6f4c9aa34a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "3c9ba3ae-60f8-450e-a4c5-d328eaa22093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "366cfbd7-622a-4e70-8a60-f6cd3970a4b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "74ad0f90-1eeb-4c33-8019-98b7881add2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "e06f4e4a-39ca-40c2-bb12-0bec1a026cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "fa0795e0-6363-4051-9e3a-2ba0bdf46d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "a9bc3125-e8a8-4d04-9825-98afc1336202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "a35527c2-3a14-414d-801b-258931a18330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "83ae7e72-04d7-4ea9-8d67-789276b81e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "f791d0e5-5801-407c-a7c2-6559d01027f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "41b57669-e05c-4346-8de7-b0700bb2a1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "bc07b0f3-e059-4117-a080-dc54a1e069d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "89f56058-5d6c-41f8-a95a-63552eb0cf3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "7bbaaeef-f7cd-4805-b6a0-000c108205ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "57cd67cb-32e2-4e39-a5e6-57bc26742869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "e517f0e2-4b46-413a-a603-be0c5fb6ceca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "53c9def5-cb57-4392-94d8-a84966276ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "9a0120c8-97b2-4899-95d6-85ce5d8af663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "b14d611f-191a-436a-baf1-c34fa0351016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "076bc8f3-13be-4b08-80b0-c3c00f66c8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "81d1786c-d130-43ce-83b2-1dbb10b7ada5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "73a4e4b0-b42e-42fe-9bdb-fd0901a08e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "9d2b9f40-e261-4b09-9802-488e80eda744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "6795bea3-c5db-4b0a-9ade-609b9060e29a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "55dd3da4-fd91-49f2-8793-84a3c0b263b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "ffd25e10-3655-41fa-ab20-ec7c85ecd674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "85522565-566b-49b5-ade0-4027997e74d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "c69133b9-b7c9-4c39-acc9-9ee967775f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "e73ff691-3d01-47f1-898e-c1f7db28d93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "50025fb9-6010-4406-a526-26c4a3206c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "998f6fea-958a-4f43-84ec-7d826c0cc45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "25331f7d-7adf-4be4-a694-e02e5276dc9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "4277c03e-368a-4722-8f0a-2e75573a4f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Italic",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}