{
    "id": "f2456fd5-17b5-4a2b-aae9-edf532777086",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fMenuArial1",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "36f94976-4525-4503-b4cd-88fa1af34b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e4900566-02db-4dd0-89cf-872fdbf4c617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 123,
                "y": 36
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b879fd88-bf9f-4ec4-87b3-d50e7d6778d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 15,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "86048796-294a-46ab-8884-50629682ed3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f572c0d8-24bf-4d9b-af01-df2581a01885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 36
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fdd1a74a-8915-492c-b0e3-714d770d886e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 83,
                "y": 36
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fcf6a21a-ad3a-4ef6-8b73-cf5e8d2bccfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 71,
                "y": 36
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ebbd17cd-9429-4cd3-a01b-f9ecbde393d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 15,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 66,
                "y": 36
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "348abae0-4c3a-4d51-8551-ea3975ee1835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "515f762a-2806-4627-b53a-08b1e2056b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 54,
                "y": 36
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "93cb6889-5ba6-4a73-80e9-0b7b409588fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 128,
                "y": 36
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "654ee044-abc3-4c05-8417-296489d670ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 44,
                "y": 36
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e1d4f596-4db5-4a8c-90d4-03757c5e8ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 30,
                "y": 36
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "3e02f8a6-d5b6-4bd1-9cf8-b84241a68469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 36
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2074a16a-eb64-4fb0-a1f0-1d71e59f1770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 36
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8dd25136-0439-4e3a-acbd-c0d3035bed0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 11,
                "y": 36
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ab9543fb-4ef4-4ab7-90bc-eeab15f55b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5e1fc78a-9f54-479a-ae16-2dbd26e4baff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 240,
                "y": 19
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f4c455d7-090c-4f3d-8a9c-da0e77336536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 231,
                "y": 19
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2bdb5e17-62d8-4851-800b-9b0424c1e9b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 222,
                "y": 19
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "80a7593d-8ed7-47ff-bc63-bfc9d6ad80af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 213,
                "y": 19
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "67414a5d-d12e-45bc-9181-25dde604534c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "afbceea0-ced2-40e0-87a0-5f1c4995c590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e3b3e75c-f9b1-4f20-ad88-c074819d43c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 144,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "aeb947bc-33e9-4704-991d-d13483fb76bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 153,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d4b533a8-9ce7-4e0b-9773-a5a8a37c6f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 53
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "392d03eb-33ce-4935-b6de-7fa9e083e5e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 53
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "33d28843-fb62-4940-b9ea-2a04c9abbb41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 53
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "aac2836a-c7be-478f-805d-3e6c434d6755",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f210f187-ffef-4c25-b786-af89b02165a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 53
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "54deac34-b28f-4d1b-a099-414325a115da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 53
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "af66017a-14cb-4e4e-b2e9-47f7fc5cde8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6749c449-5a98-4c55-b31d-1ac8c12dd98d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 15,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 47,
                "y": 53
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b7b5595f-bb3b-4784-a599-0cd400ef09c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 35,
                "y": 53
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7a5cd0c8-dd09-4072-98f5-aa505bbb0113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 53
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "48ff4687-46e0-407b-bf3e-f71debfc09d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d806d152-6c35-48d4-9fe4-8c60654bc7d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2b2adff0-2c7d-4585-8835-63aa68cd4636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 242,
                "y": 36
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0a259b78-9dda-4d0b-bce3-a3d453e9a388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 36
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "63f2fa80-46d1-4bed-8a1f-14d086d7bce3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 220,
                "y": 36
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cc52ce36-7015-4596-905a-5c14adc03cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 209,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "18bc7f75-cd68-48fa-b335-1e596e9f3c89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 36
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5d50ba1b-566f-455b-a1e1-aaeec36f8690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 195,
                "y": 36
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ce70c229-5dcd-43cb-93cd-41ac50655d46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 183,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a73074dd-f5b3-4507-bde7-2067fbc68399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 15,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 174,
                "y": 36
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "839fd5f6-9c75-4a8e-a785-cb69fa8c27de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 15,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 162,
                "y": 36
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "038d47b4-aedb-44b3-8086-d06f717917a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 202,
                "y": 19
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d08b914e-f01d-4dfc-8ac6-7d1a1dff9300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 19
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "48e9fb15-fe80-40e9-b16a-cae913aadcf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 179,
                "y": 19
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "be1e55ec-bfd8-494e-bc3e-a7ab4a0180cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b0f8eed8-3c38-4c51-9956-7752f5479fea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "637f2e8d-f586-4e32-ad4c-4eabdf7df48b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e1a8e4f8-f77a-4208-960d-949622c30103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ad9e7085-effa-4670-be3f-ac7c68192455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "439459cd-cb56-401b-b77f-8a7a28ed1dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "516f60aa-ac7e-45af-b3a0-c5766f4452e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7ea7f185-f737-47e7-9c9f-75efbbf68b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 15,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d71f2986-30a9-4572-8199-2c9585e85c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 15,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c87b631d-cb53-4253-a45f-388d3383aa4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f4b47897-697e-47c4-a353-371978d1d747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "6544b2f7-1b3a-4737-b40a-27c786a9e9d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bb2a165d-94ec-4dff-acd3-68a1c019b9c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "95ca8ca6-2eea-467e-9310-21354951ce92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "392a2644-133e-495c-a02e-54743267e725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 15,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2de4de3c-c578-4667-bca9-c8194fb10cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b61d7a1b-ca4f-4857-a6a6-21b97a239a05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d7a23472-c259-43b3-81c5-462791ef5e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2864f68d-773e-4473-93ac-d0079820507f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7c1d412f-b5b3-4088-b9f6-a8c62b5b6c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2b82ab7f-f787-409c-a1a2-44ca2ab6316b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e9f4cd46-2375-4a99-be37-020287c432e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "36d70f8a-6644-4720-b0d6-54f6ecf0d21f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "efd34a91-f318-4694-ba49-b90264b926fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 19
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a1c9280c-ce22-4067-bc3b-8c8e30df4e5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7523d47e-5f3a-453b-ba46-3908806c76e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 15,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 164,
                "y": 19
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8ba5fe52-f8b0-4145-bd19-6ce619faf6fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 154,
                "y": 19
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8cf742ac-6092-41e2-8659-1d54efa848b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 149,
                "y": 19
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5fb02b16-f0d7-40c3-9f91-e55371fbbd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 15,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 136,
                "y": 19
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4cbc3c45-53d9-4360-9eb1-8c495aef0559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 126,
                "y": 19
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fe48e2e2-9f4f-44dc-8189-bd2f10d7c99e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 19
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6566ebdd-9330-4c4b-bb47-78fcffffbf7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 19
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e30960ab-004f-43ab-a686-a97981e75354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 96,
                "y": 19
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "51f7a6c3-ca5c-4bf8-9206-fb7e1e41cf26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 88,
                "y": 19
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d9f52b23-a7a8-4158-9a4b-3fb664ea42f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 170,
                "y": 19
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a087db90-b10f-476f-a076-b6a60a8eddcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 15,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 81,
                "y": 19
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "314ea8cb-c215-4b23-9ab6-1316f2b8f4e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 19
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "fc57e179-8bd3-4de4-a466-6ee29948a66a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 51,
                "y": 19
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8eee137d-9de2-43f3-a6d2-57cb11799af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 15,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 38,
                "y": 19
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a824860e-1204-48d9-9e8b-6fe4dc37b39d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 28,
                "y": 19
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d3ed888a-b6ed-40e7-8e13-54dd7005765e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 18,
                "y": 19
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c4da4d04-cc6f-4d9f-9ffa-1948fa6a378e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 9,
                "y": 19
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b381018e-b4a9-4bf8-9e38-aa6dcd12a5ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 19
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9fdc124f-dd28-400a-b729-dd26297a4774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 15,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5b942b35-2ed2-495c-acd6-b5ad9a6c3542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 15,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "82e1f5c6-29dc-48dd-bbc5-ad82490f304b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 15,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 119,
                "y": 53
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "755f8f1c-aaaa-4b22-84a2-27d27c082087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 15,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 129,
                "y": 53
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "1c806682-4b0a-4047-9679-c54e995c9cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "97905795-c323-4dac-889c-b1de90b84eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "d022094a-cfb8-4ad2-a78f-5392ab91a86a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "88d77691-aafc-4c5d-985d-89b177d46002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "9e095000-21e6-4b9b-bad1-c53a78d3a529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "2644fdbd-2da8-42ac-be78-163f48956bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "8b663e40-271b-481b-8ebc-0808e76ced51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "d230a1a5-10dc-4400-a68e-7fe722522fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "dbe3c377-8544-4e3a-b953-70fefafeb5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "2462c4be-ac96-4f3f-a3d6-45cd121b1517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "aeb1a9c8-cc45-4d5f-991c-c0dae48f37f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "5ae6a1ce-5949-4af2-8bf4-18a8ca1ba8f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "58338b4f-81af-41df-9be0-63ac3c158fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "6ee2a672-ea32-47ab-af7a-059405f70c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "bff1bcb8-9f58-407f-ad7e-895b1e8c00ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "52de8415-1eeb-4c19-9773-47bdfa925fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "5a26da80-d0a2-4d7a-aff9-dc6b75750dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "c6235635-ad9f-4dec-b262-035df79ad446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "d98f8c94-eae1-4c63-92fe-5a3ec64c33cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "dae85825-aabc-48a1-a40e-38e79e694bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "6551818c-2427-4367-82a7-ab7e0ca61955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "8f2ec5bb-80d5-4aa4-bad7-f68cae8e85af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "05b745c9-0f6f-4531-94c2-0471a5b1f732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "de57145d-010e-4aeb-be0f-a153f3b1ea5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "4d8c9344-f9e9-4b8f-8f8f-503b8d16944d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "bd6e83cf-7713-4d4c-9f09-c6b9fbbd0e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "f92f596d-8df4-48d0-ad85-08a7bb600299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "eeda97c6-3a04-4b09-b473-1bfe61482c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "8e371725-56f2-4afe-99d9-78a960c78465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "5328c7ae-92ea-4c4d-902c-aba8df0c9698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "03031d5e-e6e4-43fc-a677-b4f5bc6744fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "250fc8b3-1b0e-4d9b-a765-85e318a4202b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "50fe255f-80a2-459d-9345-394eaaef1db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "462e73c5-4b30-45ad-897b-56d3ae6799f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "c13004f2-5c2d-482d-af98-ca5997883cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "6d0256fb-f405-4261-ab7a-4d84f351ef24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "78e1e216-052a-4a9d-a5bf-f478522d8bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "522fd57f-f04b-4f50-9870-e35d346d8d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "ab0f5eee-2976-41bc-a88e-1c6c546cda48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "eead37ba-5afb-4246-8c50-bd66b90d1d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "7e2bb60d-1dc1-4296-8052-4844f73b0858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}