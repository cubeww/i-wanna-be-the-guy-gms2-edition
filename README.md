# I Wanna Be The Guy GMS2 Edition

本游戏是由Kayin制作的I Wanna Be The Guy进行复刻，使用YoYoYoDude的GameMaker Studio 2引擎重新制作而成。

目前进度：

* 开场动画和主菜单

## 从CTF2.5到GMS2的一些笔记

* GMS2中在**精灵处**编辑的帧速率为CTF2.5的**1/2**
* GMS2的**对象运动速度**为CTF2.5的**1/2**