{
    "id": "9f2ed53a-746a-4a3c-bfe3-da91b2a23931",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuCloudSpawner",
    "eventList": [
        {
            "id": "3f0d3a18-0990-4277-a093-c1b800e13791",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f2ed53a-746a-4a3c-bfe3-da91b2a23931"
        },
        {
            "id": "c05573e9-4efe-4ccc-9ce9-803fb67499a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9f2ed53a-746a-4a3c-bfe3-da91b2a23931"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7ea90e1-21eb-4752-b63f-d53130920655",
    "visible": true
}