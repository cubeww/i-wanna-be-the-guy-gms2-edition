{
    "id": "51119ff8-3739-4f2a-9e22-00208131f3a2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuCloud",
    "eventList": [
        {
            "id": "98e46894-34cb-4996-93a0-998bf0e75983",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51119ff8-3739-4f2a-9e22-00208131f3a2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f7ea90e1-21eb-4752-b63f-d53130920655",
    "visible": true
}