/// @description 控制视野移动
var vsp = 3;
if (move)
{
	camera_set_view_pos(view_camera[0], 0, camera_get_view_y(view_camera[0]) - vsp);

	if (camera_get_view_y(view_camera[0]) <= 0)
	{
		// 视野移动完毕
		camera_set_view_pos(view_camera[0], 0, 0);
		move = false;
		
		alarm[0] = 100;
	}
}