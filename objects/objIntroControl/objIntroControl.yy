{
    "id": "1ff99ac1-5e92-45b3-a981-8c7564399898",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objIntroControl",
    "eventList": [
        {
            "id": "8f96a472-7f33-47c9-b63f-8bf34dec4bf2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ff99ac1-5e92-45b3-a981-8c7564399898"
        },
        {
            "id": "be6b6aac-e12d-41b0-9d13-d4794fa0e71f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1ff99ac1-5e92-45b3-a981-8c7564399898"
        },
        {
            "id": "7dfd7ec4-7f1c-4555-80a1-3c078513a7c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "1ff99ac1-5e92-45b3-a981-8c7564399898"
        },
        {
            "id": "940f8a02-da90-403b-8572-f33da429c693",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1ff99ac1-5e92-45b3-a981-8c7564399898"
        },
        {
            "id": "ca40be75-bae1-4854-8686-3650bfc9c28f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "1ff99ac1-5e92-45b3-a981-8c7564399898"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}