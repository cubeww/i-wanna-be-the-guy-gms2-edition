{
    "id": "4369eb9c-a5f0-4e2f-8da5-67ed094d135f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuTree",
    "eventList": [
        {
            "id": "482846ab-0df5-4e78-8f0c-da1534d9eebe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4369eb9c-a5f0-4e2f-8da5-67ed094d135f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ef0a337-86b2-43e8-bf21-c870f4a26e63",
    "visible": true
}