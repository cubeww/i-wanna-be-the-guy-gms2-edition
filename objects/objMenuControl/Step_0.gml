/// @description 按键检测
// 选择档位
if (select2 == -1)
{
	if (scrButtonCheckPressed(global.rightButton))
	{
		select += 1;
		if (select == 3) select = 0;
	}
	if (scrButtonCheckPressed(global.leftButton))
	{
		select -= 1;
		if (select == -1) select = 2;
	}
}
// 进入二级菜单 | 选难度
if (scrButtonCheckPressed(global.jumpButton))
{
	if (select2 == -1)
		select2 = 0;
	else 
	{
		if (select2 == 0)
		{
			// 读档
			if (file_exists("Data\\save"+string(select + 1))) {
                scrLoadGame(true);
			}
		}
		else if (select2 == 1)
		{
			// 开始新游戏
            global.gameStarted = true; 
            global.autosave = true;
            global.saveNum = select + 1;
            global.difficulty = select3;
                        
            room_goto(global.startRoom);
		}
	}
}
// 返回二级菜单
if (scrButtonCheckPressed(global.shootButton))
{
	select2 = -1;
	select3 = 1;
}
// 二级菜单选择
if (select2 != -1)
{
	if (scrButtonCheckPressed(global.downButton))
	{
		select2 = clamp(select2 + 1, 0, 1);
	}
	if (scrButtonCheckPressed(global.upButton))
	{
		select2 = clamp(select2 - 1, 0, 1);
	}
	// 选难度
	if (select2 == 1)
	{
		if (scrButtonCheckPressed(global.rightButton))
		{
			select3 = clamp(select3 + 1, 0, 3);
		}
		if (scrButtonCheckPressed(global.leftButton))
		{
			select3 = clamp(select3 - 1, 0, 3);
		}
	}
}
arrowIndex = (arrowIndex + 0.5) mod 6;