/// @description 绘制菜单界面

// 顶端文字
draw_set_color(c_white);
draw_set_font(fMenuBig);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_text(379, 48, "* Select Your Saved Game *");

// 顶端灰色方框
draw_set_color(c_gray);
draw_rectangle(221, 32, 221 + 316, 32 + 32, true);

// Box
for (var i = 0; i < 3; i++)
{
	var xx, yy;
	xx = 64 + i * 272; // 每个Box黑色部分起点X位置
	yy = 160; // 每个Box黑色部分起点Y位置
	
	if (i == select)
	{
		// 选中时高亮显示
		draw_sprite(sprMenuBoxLight, 0, xx - 32, yy - 32);
	}
	
	if (select2 == -1 || select != i) // 未进入二级选择
	{
		// 存档预览图像
		if (screenshot[i] == -1)
			draw_sprite(sprMenuNewGame, 0, xx, yy + 32);
		else
			draw_sprite_stretched(screenshot[i], 0, xx, yy + 32, 128, 96);
	}
	else if (select == i) // 二级选择
	{
		// 箭头
		draw_sprite(sprTitleArrow, arrowIndex, xx + 8, yy + 40 + 32 * select2);
		
		// 文字指示
		draw_set_font(fMenuArial);
		draw_set_color(c_white);
		draw_set_halign(fa_left);
		draw_set_valign(fa_middle);
		draw_text(xx + 32, yy + 48, "Continue");
		draw_text(xx + 32, yy + 80, "New Game");
		// 三级菜单
		draw_set_halign(fa_center);
		var difName;
		switch (select3)
		{
			case 0: difName = "Medium"; break;
			case 1: difName = "Hard"; break;
			case 2: difName = "Very Hard"; break;
			case 3: difName = "Impossible"; break;
		}
		draw_set_font(fMenuItalic);
		draw_text(xx + 80, yy + 96, "<-(" + difName + ")->");
		// 灰色方框
		draw_set_color(c_gray);
		draw_rectangle(xx + 32, yy + 32, xx + 128, yy + 64, true);
		draw_rectangle(xx + 32, yy + 64, xx + 128, yy + 112, true);
	}
	
	
	// SAVE GAME 文字
	draw_set_color(c_white);
	draw_set_font(fMenuMiddle);
	draw_set_halign(fa_center);
	draw_text(128 + i * 272, 176, "SAVE GAME " + string(i + 1));
	
	// SAVE GAME 灰色方框
	draw_set_color(c_gray);
	draw_rectangle(xx, yy, xx + 128, yy + 32, true);
	
	// 小文字
	draw_set_font(fMenuSmall);
	draw_set_halign(fa_left);
	draw_set_color(c_white);
	// Playtime 文字
	yy = 304;
	draw_text(xx, yy, "Playtime: " + timeStr[i]);
	// Deaths 文字
	yy += 32;
	draw_text(xx, yy, "Deaths: " + string(deaths[i]));
	// Difficulty 文字
	yy += 32;
	var difName;
	difName = "";
	if (exists[i])
	{
		switch (difficulty[i])
		{
			case 0: difName = "Medium"; break;
			case 1: difName = "Hard"; break;
			case 2: difName = "Very Hard"; break;
			case 3: difName = "Impossible"; break;
		}
	}
	draw_text(xx, yy, "Difficulty: " + difName);
	// 其余黑色方框
	draw_set_color(c_gray);
	for (var j = 0; j < 3; j++)
	{
		draw_rectangle(xx, 288 + j * 32, xx + 128, 288 + j * 32 + 32, true);
	}
}
