{
    "id": "8bea4593-c122-49e0-ba77-ae4b2f8e2fe5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuControl",
    "eventList": [
        {
            "id": "303320af-c4fa-462b-8638-c4f1d5d14395",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8bea4593-c122-49e0-ba77-ae4b2f8e2fe5"
        },
        {
            "id": "23e32409-1705-408a-a64d-7305f60c8095",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "8bea4593-c122-49e0-ba77-ae4b2f8e2fe5"
        },
        {
            "id": "d0493cc2-adcf-4cd8-9ed1-775d9e3f0e3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8bea4593-c122-49e0-ba77-ae4b2f8e2fe5"
        },
        {
            "id": "1595a961-2bde-4acc-b608-0f903a558863",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "8bea4593-c122-49e0-ba77-ae4b2f8e2fe5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}