/// @description 制作反色屏幕表面
// 绘制反色屏幕到表面
surface_set_target(surf);

shader_set(shd_invert);
draw_surface(application_surface, 0, 0);
shader_reset();

surface_reset_target();