{
    "id": "798df6ed-076b-4104-a68b-5e4cbb55411a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTitleAControl",
    "eventList": [
        {
            "id": "37a50473-4e62-4a25-acde-0af31be931b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "798df6ed-076b-4104-a68b-5e4cbb55411a"
        },
        {
            "id": "aadef119-e423-4780-9f61-9cdc104aafbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "798df6ed-076b-4104-a68b-5e4cbb55411a"
        },
        {
            "id": "98e40599-db15-4847-861f-f14a9377daa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "798df6ed-076b-4104-a68b-5e4cbb55411a"
        },
        {
            "id": "a0333ae0-2570-464c-b603-69e9a5707dde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "798df6ed-076b-4104-a68b-5e4cbb55411a"
        },
        {
            "id": "cb74e3bf-ca0a-4618-bef4-ef52c9689c40",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "798df6ed-076b-4104-a68b-5e4cbb55411a"
        },
        {
            "id": "fb0a3d2e-a4f9-4252-9594-f4a52cc35092",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "798df6ed-076b-4104-a68b-5e4cbb55411a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}