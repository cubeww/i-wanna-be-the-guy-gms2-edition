/// @description 绘制屏幕特效

// 反色屏幕绘制
if (alpha != 0)
{
	draw_surface(surf, 0, 0)
}
// 黑屏效果
draw_set_alpha(alpha);

draw_set_color(c_black);
draw_rectangle(0, 0, 800, 608, 0);

draw_set_alpha(1);