/// @description 控制事件
/*
	播放BGM---------按下Shift
		|				|
	BGM结束				|------------------|
		|			进入选难度界面		BOSS RUSH
	制作屏幕反色表面
		|
	绘制反色屏幕表面
		|
	绘制黑暗效果
		|
	过场动画
*/

// 获取BGM时间长度
var len = audio_sound_length(musTitleA);
// 设定时间产生屏幕特效
alarm[0] = len * 50;
alarm[1] = alarm[0];
// 绘制黑屏的透明度
alpha = 0;
// 绘制用表面
surf = surface_create(800, 608);
