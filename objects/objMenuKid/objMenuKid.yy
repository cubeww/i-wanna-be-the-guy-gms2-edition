{
    "id": "105f405a-251a-45cc-976b-bb99d01f08d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuKid",
    "eventList": [
        {
            "id": "17632d52-dfe4-48c2-af3a-5ffc7ef2d51c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "105f405a-251a-45cc-976b-bb99d01f08d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c93dbbe-3f2e-400e-b144-71d00406e30e",
    "visible": true
}