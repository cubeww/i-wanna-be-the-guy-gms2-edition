{
    "id": "b077470e-56ba-4c82-9b20-52834151199b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTitleBControl",
    "eventList": [
        {
            "id": "2e6f9731-54b4-4151-b565-8b94c154922f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b077470e-56ba-4c82-9b20-52834151199b"
        },
        {
            "id": "5a9879b3-8971-48f7-a6ee-c9ee9a80db64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b077470e-56ba-4c82-9b20-52834151199b"
        },
        {
            "id": "7df1407c-dc8e-476c-809b-b0d339d1b74d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "b077470e-56ba-4c82-9b20-52834151199b"
        },
        {
            "id": "57021fdf-0a36-4a5f-9d1a-df93e99ce5b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b077470e-56ba-4c82-9b20-52834151199b"
        },
        {
            "id": "885624e0-162d-4eb9-922d-aeff5cadea0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b077470e-56ba-4c82-9b20-52834151199b"
        },
        {
            "id": "1baba734-5288-41c4-979f-eafa737a97a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "b077470e-56ba-4c82-9b20-52834151199b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}