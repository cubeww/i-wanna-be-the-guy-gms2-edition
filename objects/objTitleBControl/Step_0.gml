/// @description 移动视野
var vsp = 1;
if (move)
{
	camera_set_view_pos(view_camera[0], 0, camera_get_view_y(view_camera[0]) + vsp);

	if (camera_get_view_y(view_camera[0]) >= room_height - camera_get_view_height(view_camera[0]))
	{
		// 视野移动完毕
		camera_set_view_pos(view_camera[0], 0, room_height - camera_get_view_height(view_camera[0]));
		move = false;
	}
}
