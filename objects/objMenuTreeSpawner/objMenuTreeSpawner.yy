{
    "id": "0d47f403-3119-479e-9d82-66be46ba32f9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMenuTreeSpawner",
    "eventList": [
        {
            "id": "4b7d946a-45bb-4576-9928-510fede2d9f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d47f403-3119-479e-9d82-66be46ba32f9"
        },
        {
            "id": "4bb39e4f-0010-4fab-a4e7-e277e2d85986",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d47f403-3119-479e-9d82-66be46ba32f9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4ef0a337-86b2-43e8-bf21-c870f4a26e63",
    "visible": true
}