/// @description 绘制版本号
draw_set_font(fDefault12);
draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_valign(fa_middle);
draw_text(x,y + 16,global.versionString);

draw_set_color(c_gray);
draw_rectangle(x, y, x + 227, y + 32, true);
